<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "driver_tariff".
 *
 * @property integer $id
 * @property integer $driver_id
 * @property string $town
 * @property string $town_center
 * @property string $km_price
 * @property string $periferia
 * @property boolean $use_single_tariff
 */
class DriverTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id'], 'integer'],
	        [['driver_id'], 'required'],
            [['town_center', 'periferia', 'km_price'], 'string', 'max' => 20],
            [['town'], 'string', 'max' => 256],
            [['use_single_tariff'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'driver_id' => 'Водитель',
            'town' => 'Уфа',
            'town_center' => 'Уфа(центр)',
            'periferia' => 'Периферия',
            'km_price' => 'Цена за км.',
	        'use_single_tariff' => 'Использовать как единый тариф'
        ];
    }
}
