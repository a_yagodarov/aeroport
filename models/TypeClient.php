<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "type_client".
 *
 * @property integer $id
 * @property string $name
 */
class TypeClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 64],
	          [['name'], 'required']
        ];
    }

		public static function getTypeClientsForDropDownList()
		{
			return ArrayHelper::map(self::find()->all(), 'id', 'name');
		}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
        ];
    }
}
