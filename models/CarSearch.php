<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Car;

/**
 * CarSearch represents the model behind the search form about `app\models\Car`.
 */
class CarSearch extends Car
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class', 'year', 'number_of_passengers'], 'integer'],
            [['id', 'model', 'number', 'color', 'company'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Car::find();

        // add conditions that should alTransfers apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

	    if ($this->company)
	    {
//		    $query->leftJoin(
//			    'car_brand',
//			    'car.brand_id = car_brand.id'
//			    );
//		    $query->orFilterWhere([
//			    'like', 'car_brand.name', '%'.$this->company.'%', false
//		    ]);
		    $query->orFilterWhere([
			    'like', 'number', '%'.$this->company.'%', false
		    ]);
		    $query->orFilterWhere([
			    'like', 'model', '%'.$this->company.'%', false
		    ]);
		    $query->orFilterWhere([
			    'like', 'color', '%'.$this->company.'%', false
		    ]);
	    }
        // grid filtering conditions
//        $query->andFilterWhere([
//            'company' => $this->company,
//            'class' => $this->class,
//            'year' => $this->year,
//            'number_of_passengers' => $this->number_of_passengers,
//        ]);
//
//        $query->andFilterWhere(['like', 'model', $this->model])
//            ->andFilterWhere(['like', 'number', $this->number])
//            ->andFilterWhere(['like', 'color', $this->color]);

        return $dataProvider;
    }
}
