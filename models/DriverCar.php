<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "driver_car".
 *
 * @property integer $id
 * @property integer $driver_id
 * @property integer $car_id
 *
 * @property Car $car
 * @property Driver $driver
 */
class DriverCar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver_car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id', 'car_id'], 'integer'],
	        [['mifar_card_id'], 'string', 'min' => 6, 'max' => 20],
	        [['mifar_card_id'], 'required'],
	        [['mifar_card_id'], 'unique'],
	        [['car_id', 'mifar_card_id'], 'required', 'on' => 'driver_create'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
	        ['driver_id', 'required', 'when' => function($model, $attribute){
		        if ($this->findOne([
			        'car_id' => $this->car_id,
			        'driver_id' => $this->driver_id
		        ]))
		        $this->addError($attribute, 'Такой водитель уже добавлен');
//		        return true;
	        }, 'on' => 'create']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'driver_id' => 'Водитель',
            'car_id' => 'Автомобиль',
	        'mifar_card_id' => 'Номер карты стандарта "Mifare1"'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }

	public static function getFullInfo($id)
	{
		$driverCar = DriverCar::findOne([
			'mifar_card_id' => $id
		]);
		if (!$driverCar)
		{
			return false;
		}
		$car = $driverCar->getCar()->one();
		$driver = $driverCar->getDriver()->one();
		/*
		 *
			 company
			 model
			 color
			 number
			 image

			 full_name
			 serial_number
			 town_center
			 town
			 km_price
		 */

		$object = new \stdClass();
		if ($car->use_group_tariff && $car->group_tariff_id)
		{
			$groupTariff = GroupTariff::findOne([
				'id' => $car->group_tariff_id
			]);
			$object->town = $groupTariff->town;
			$object->town_center = $groupTariff->town_center;
			$object->periferia = $groupTariff->periferia;
			$object->km_price = $groupTariff->km_price;
		}
		else
		{
			$driverTariff = DriverTariff::findOne([
				'driver_id' => $driver->id
			]);
			$object->town = $driverTariff->town;
			$object->town_center = $driverTariff->town_center;
			$object->periferia = $driverTariff->periferia;
			$object->km_price = $driverTariff->km_price;
		}
		$object->company = $car->getCarBrand();
		$object->model = $car->model;
		$object->color = $car->color;
		$object->number = $car->number;
		$object->image = Yii::getAlias(Yii::$app->request->hostInfo.'/web/files/images/drivers/'.$driver->photo_image);
		$object->full_name = $driver->getFullName();
		$object->serial_number = $driver->serial_number;
		return $object;
	}
}
