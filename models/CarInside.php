<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "car_inside".
 *
 * @property integer $id
 * @property integer $car_id
 * @property string $image
 *
 * @property Car $car
 */
class CarInside extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_inside';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'jpg, jpeg, png'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::className(), 'targetAttribute' => ['car_id' => 'id']],
	        [['car_id', 'image'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_id' => 'Car ID',
            'image' => 'Фото',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }

	public function uploadImage()
	{
		if (!$this->image)
			return false;
		$path = Yii::getAlias('@app/web/files/images/'.$this->car_id.'/');
		if (!is_dir($path)) mkdir($path, 0777, true);
		$filename = Yii::$app->security->generateRandomString(9);
		$this->image->saveAs($path . $filename . '.' . $this->image->extension);
		$this->image = $filename . '.' . $this->image->extension;
		$this->getImagePath();
		return true;
	}

	public function getImage()
	{
		$path = Yii::getAlias('/web/files/images/'.$this->car_id.'/'.$this->image);
		$path2 = Yii::getAlias('@app/web/files/images/'.$this->car_id.'/'.$this->image);
		if (file_exists($path2))
		{
			$size = filesize($path2);
			if ($size > 100000)
			{
				$simpleImage = new SimpleImage();
				$simpleImage->load($path2);
				$simpleImage->resizeToWidth(200);
				$simpleImage->save($path2);
			}
		}
		return Html::img($path, [
			'height' => '100px',
			'width' => 'auto'
		]);
	}

	public static function getImageByName($id, $image)
	{
		$path = Yii::getAlias('/web/files/images/'.$id.'/'.$image);
		$path2 = Yii::getAlias('@app/web/files/images/'.$id.'/'.$image);
		if (file_exists($path2))
		{
			$size = filesize($path2);
			if ($size > 100000)
			{
				$simpleImage = new SimpleImage();
				$simpleImage->load($path2);
				$simpleImage->resizeToWidth(200);
				$simpleImage->save($path2);
			}
		}
		return Html::img($path, [
			'height' => '100px',
			'width' => 'auto'
		]);
	}

	public static function getImagePathByName($id, $image)
	{
		$path2 = Yii::getAlias('@app/web/files/images/'.$id.'/'.$image);
		if (file_exists($path2))
		{
			$size = filesize($path2);
			if ($size > 100000)
			{
				$simpleImage = new SimpleImage();
				$simpleImage->load($path2);
				$simpleImage->resizeToWidth(200);
				$simpleImage->save($path2);
			}
		}
		return $path = Yii::getAlias('/web/files/images/'.$id.'/'.$image);
	}

	public function deleteImage()
	{
		$path = Yii::getAlias('@app/web/files/images/'.$this->car_id.'/'.$this->oldAttributes['image']);
		if (file_exists($path))
			unlink($path);
	}
}
