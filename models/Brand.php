<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "brand".
 *
 * @property integer $id
 * @property string $image
 * @property string $name
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'string', 'max' => 64],
            [['name'], 'string', 'max' => 32],
	        [['image', 'name'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Логотип(изображение)',
            'name' => 'Название'
        ];
    }

	public static function getBrandsForDropDown()
	{
		return ArrayHelper::map(self::find()->all(), 'id', 'name');
	}

	public function uploadImage()
	{
		if (!$this->image)
			return false;
		$path = Yii::getAlias('@app/web/files/images/brand/');
		if (!is_dir($path)) mkdir($path, 0777, true);
		$filename = Yii::$app->security->generateRandomString(9);
		$this->image->saveAs($path . $filename . '.' . $this->image->extension);
		$this->image = $filename . '.' . $this->image->extension;
		return true;
	}

	public function getImage()
	{
		$path = Yii::getAlias('/web/files/images/brand/'.$this->image);
		return Html::img($path, [
			'height' => '100px',
			'width' => 'auto'
		]);
	}

	public function deleteImage()
	{
		$path = Yii::getAlias('@app/web/files/images/brand/'.$this->oldAttributes['image']);
		if (file_exists($path))
			unlink($path);
	}
}
