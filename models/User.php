<?php
namespace app\models;

use Yii;
use dektrium\user\models\User as BaseUser;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * @property integer $role_id
**/

class User extends BaseUser
{
	const ROLE_ADMIN = 1;
	const DISPATCHER = 2;
	const ROLE_MANAGER = 3;
	public $info;

	public static function getAllUsersForDropDownListByRole($roleName) {

		$roleData = Role::findOne(["name"=>$roleName]);

		$data = [];
		if($roleData)
			$data = ArrayHelper::map(User::find()->where(["role_id"=>$roleData->id])->asArray()->all(),'id','username');

		return $data;
	}

	public static function getNameById($id) {
		$row = self::find()->andWhere('id = :userId', array('userId' => $id))->one();
		if($row)
			return $row->username;
		return '';
	}
	public function scenarios()
	{
		$scenarios = parent::scenarios();
		// add field to scenarios
		array_merge($scenarios['create'], ['role_id', 'admin_id']);
		array_merge($scenarios['update'], ['role_id', 'admin_id']);
		array_merge($scenarios['register'], ['role_id', 'admin_id']);
		return $scenarios;
	}

	public function rules()
	{
		$rules = parent::rules();
		// add some rules

		$rules['fieldRequired'] = [['role_id'], 'required'];
		$rules['fieldLength']   = [['role_id'], 'number'];
		$rules[] = [['terminal_id'], 'required', 'when' => function($model){
			return ($model->role_id == self::ROLE_MANAGER);
		}];
//		$rules[] = [['role_id'], 'required', 'when' => function($model){
//			if ($this->oldAttributes['role_id'] == self::ROLE_ADMIN)
//				$this->addError('role_id', 'Нельзя ')
//		}];
		return $rules;
	}

	public function attributeLabels()
	{
		$arr = [
			'email' => Yii::t('app', 'Email'),
			'role_id' => Yii::t('app', 'Role'),
			'terminal_id' => 'Терминал(для менеджера)'
		];
		return array_merge(parent::attributeLabels(), $arr);
	}

	public static function getUserRolesDropdownlist()
	{
		$arr = [
			self::DISPATCHER => 'Оператор въезда',
			self::ROLE_MANAGER => 'Менеджер',
//			self::ROLE_ACCOUNTANT => Yii::t('app', 'Accountant'),
		];
		return $arr;
	}

	public static function getClientRolesDropdownlist()
	{
		$arr = [
			self::LEGAL_ENTITY => Yii::t('app', 'Legal entity'),
//			self::INDIVIDUAL => Yii::t('app', 'Individual')
		];
		return $arr;
	}

	public static function getMenuItemsByRoleUser()
	{
		if (Yii::$app->user->identity->role_id == User::ROLE_ADMIN) {
			return
				[
					'options' => ['class' => 'sidebar-menu'],
					'items' => [
						['label' => 'Меню', 'options' => ['class' => 'header']],
						['label' => 'Пользователи', 'icon' => 'fa fa-users', 'url' => ['/user/admin/index']],
						['label' => 'Водители', 'icon' => 'fa fa-user', 'url' => ['/driver/index']],
						['label' => 'Автомобили', 'icon' => 'fa fa-car', 'url' => ['/car/index']],
						['label' => 'Марки', 'icon' => 'fa fa-circle', 'url' => ['/car-brand/index']],
						['label' => 'Групповые тарифы', 'icon' => 'fa fa-rub', 'url' => ['/group-tariff/index']],
						['label' => 'Бренды', 'icon' => 'fa fa-apple', 'url' => ['/brand/index']],
						['label' => 'Стоянка', 'icon' => 'fa fa-car', 'url' => ['/panel/index']],
						['label' => 'Отчеты', 'icon' => 'fa fa-bars', 'url' => ['/report/index']],
						['label' => 'Типы клиентов', 'icon' => 'fa fa-text-height', 'url' => ['/type-client/index']],
						['label' => 'Действия пользователей', 'icon' => 'fa fa-check', 'url' => ['/user-action/index']],
					],
				];
		}
		elseif(Yii::$app->user->identity->role_id == User::DISPATCHER)
		{
			return
				[
					'options' => ['class' => 'sidebar-menu'],
					'items' => [
						['label' => 'Меню', 'options' => ['class' => 'header']],
//						['label' => 'Водители', 'icon' => 'fa fa-user', 'url' => ['/driver/index']],
//						['label' => 'Автомобили', 'icon' => 'fa fa-car', 'url' => ['/car/index']],
//						['label' => 'Марки', 'icon' => 'fa fa-car', 'url' => ['/car-brand/index']],
						['label' => 'Стоянка', 'icon' => 'fa fa-car', 'url' => ['/panel/index']],
					],
				];
		}
		elseif(Yii::$app->user->identity->role_id == User::ROLE_MANAGER)
		{
			return
				[
					'options' => ['class' => 'sidebar-menu'],
					'items' => [
						['label' => 'Меню', 'options' => ['class' => 'header']],
						['label' => 'Стоянка', 'icon' => 'fa fa-car', 'url' => ['/panel/index']],
						['label' => 'Заказы', 'icon' => 'fa fa-bars', 'url' => ['/order/index']],
					],
				];
		}
	}

	public function getRole()
	{
		return $this->hasOne(Role::className(), ['id' => 'role_id']);
	}

	public function getUser_info()
	{
		return $this->hasOne(UserInfo::className(), [
			'id' => 'id'
		]);
	}

	public static function getUserListForDropDownList()
	{
		$result = (new Query())
			->select('id, username')
			->from(User::tableName())
			->all();
		$arr = [];
		foreach ($result as $item)
		{
			$arr[$item['id']] = $item['username'];
		}
		return $arr;
	}
}
