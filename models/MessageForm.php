<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 09.07.2016
 * Time: 10:32
 */
namespace app\models;

class MessageForm extends \yii\base\Model
{
	public $place;
	public $zone;
	public $panel_id;

	public function rules()
	{
		return  [
			[['panel_id', 'place'], 'integer'],
			[['panel_id', 'place', 'zone'], 'required'],
			[['panel_id'], 'exist', 'skipOnError' => false, 'targetClass' => Panel::className(), 'targetAttribute' => ['panel_id' => 'id']],
			[['zone'], 'number', 'min' => 1, 'max' => 4],
		];
	}

	public function attributeLabels()
	{
		return [
			'place' => 'Терминал'
		];
	}

	public function sendMessage()
{
	$panel = Panel::findOne([
		'id' => $this->panel_id
	]);
	if (!$panel)
		$panel = Order::findOne([
			'id' => $this->panel_id
		]);
	$message = $this->getMessage($panel);

	$soapClient = new \SoapClient('https://smsc.ru/sys/soap.php?wsdl');

	$result = $soapClient->send_sms([
		'login' => 'scorp12',
		'psw' => 'sms@ufa2016',
		'phones' => $panel->driver->phone_number,
		'mes' => $message,
		'id' => $this->panel_id,
		'sender' => '89175566606'
	]);
	if (!isset($result->sendresult->error))
		return true;
	else
		return $result->sendresult->error;
	return true;

}

	public function getMessage($panel)
	{
		return
			'Номер заказа: '.$this->panel_id.
			' тариф: "'.$this->getTariffById($panel).
			'" терминал: '.Terminal::getTerminalsForDropDownList()[$this->place];
	}

	public function getTariffById($panel)
	{
		switch ($this->zone){
			case 2:
				return 'Центр '.$panel->town_center;
			case 1:
				return 'Город '.$panel->town;
			case 3:
				return 'межгород '.$panel->km_price;
			case 4:
				return 'периферия '.$panel->periferia;
		}
	}
}