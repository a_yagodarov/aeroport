<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "group_tariff".
 *
 * @property integer $id
 * @property string $name
 * @property string $town_center
 * @property string $town
 * @property string $km_price
 * @property string $comment
 */
class GroupTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['town_center', 'km_price', 'periferia'], 'string', 'max' => 20],
            [['town'], 'string', 'max' => 256],
	        [['use_single_tariff'], 'boolean'],
            [['comment'], 'string', 'max' => 256],
	          [['town_center', 'town', 'km_price', 'name'], 'safe'],
        ];
    }

	public static function getDropDownForGroupTariff()
	{
		return ArrayHelper::map(self::find()->all(), 'id', 'name');
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'town_center' => 'Уфа(центр)',
            'town' => 'Уфа',
            'km_price' => 'Цена за км',
            'comment' => 'Комментарий',
            'periferia' => 'Периферия',
	        'use_single_tariff' => 'Использовать как единый тариф'
        ];
    }
}
