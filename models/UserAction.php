<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_action".
 *
 * @property integer $id
 * @property integer $action_id
 * @property integer $manager_id
 * @property integer $date_create
 * @property string $text
 *
 * @property User $manager
 */
class UserAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'manager_id', 'date_create'], 'integer'],
            [['action_id', 'manager_id', 'date_create'], 'required'],
	          [['text'], 'string', 'max' => 64],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['manager_id' => 'id']],
        ];
    }

		public function getActionById($id)
		{
			return $this->getActions()[$id];
		}

		public function getActions()
		{
			return [
				'1' => 'Заказ',
				'2' => 'Въезд',
				'3' => 'Выезд'
			];
		}

		public static function getActionsForDropDown()
		{
			return [
				'1' => 'Заказ',
				'2' => 'Въезд',
				'3' => 'Выезд'
			];
		}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'action_id' => 'Действие',
	          'text' => 'Текст',
            'manager_id' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }
}
