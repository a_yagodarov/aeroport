<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 04.07.2016
 * Time: 18:45
 */

namespace app\models;

class PanelDeleteForm extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */

	public static function tableName()
	{
		return 'panel';
	}

	public function rules()
	{
		return [
			['car_id', 'required']
		];
	}

	public function attributeLabels()
	{
		return [
			'car_id' => 'Автомобиль'
		];
	}
}