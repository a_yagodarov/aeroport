<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $car_id
 * @property integer $driver_id
 * @property integer $user_id
 * @property integer $use_new_tariffs
 * @property string $town
 * @property string $town_center
 * @property string $km_price
 * @property integer $station_time
 * @property integer $date_create
 * @property integer $date_update
 * @property integer $use_single_tariff
 *
 * @property Address[] $addresses
 */
class Order extends Panel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }
	
//    /**
//     * @inheritdoc
//     */
//    public function rules()
//    {
//        return [
//            [['car_id', 'driver_id', 'use_new_tariffs', 'station_time', 'date_create', 'date_update'], 'integer'],
//            [['town', 'town_center', 'km_price'], 'string', 'max' => 64],
//        ];
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'car_id' => 'Car ID',
//            'driver_id' => 'Driver ID',
//            'use_new_tariffs' => 'Use New Tariffs',
//            'town' => 'Town',
//            'town_center' => 'Town Center',
//            'km_price' => 'Km Price',
//            'station_time' => 'Station Time',
//            'date_create' => 'Date Create',
//            'date_update' => 'Date Update',
//        ];
//    }
//	public function getCar()
//	{
//		return $this->hasOne(Car::className(), ['id' => 'car_id']);
//	}
//
//	public function getBrand()
//	{
//		$brand_id = $this->car->brand_id;
//		$model = Brand::findOne(['id' => $brand_id]);
//		return $model;
//	}
//
//	public function getDriver()
//	{
//		return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
//	}
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getAddresses()
//    {
//        return $this->hasMany(Address::className(), ['order_id' => 'id']);
//    }
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['car_id', 'driver_id', 'user_id'], 'required'],
			[['car_id'], 'required', 'when' => function($model){
				$car = Car::findOne(['id' => $model->car_id]);
				if (!$car)
				{
					$this->addError('car_id', 'Такое авто не найдено!');
				}
				elseif($car->is_black_list)
				{
					$this->addError('car_id', 'Авто находится в черном списке');
				}
				elseif(!$car->permission_to_drive)
				{
					$this->addError('car_id', 'Статус авто не активирован');
				}
				else
					return true;
			}],
			[['car_id', 'driver_id', 'use_new_tariffs', 'user_id', 'use_single_tariff'], 'integer'],
			[['town', 'town_center', 'km_price'], 'string', 'max' => 64],
			[['station_time'], 'number'],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
			[['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::className(), 'targetAttribute' => ['car_id' => 'id']],
			[['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
			[['car_id'], 'unique', 'message' => 'Такое авто уже есть на стоянке'],
			[['driver_id'], 'unique', 'message' => 'Такой водитель уже на стоянке']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'car_id' => 'Автомобиль',
			'driver_id' => 'Водитель',
			'use_new_tariffs' => 'Использовать новые тарифы',
			'town' => 'Уфа',
			'town_center' => 'Уфа(центр)',
			'km_price' => 'Межгород',
			'station_time' => 'Время на стоянке(в ч.)',
			'user_id' => 'Пользователь'
		];
	}

	public function afterSave($insert, $changedAttributes)
	{
		return false;
		if ($insert)
		{
			$description = Car::getCarNameById($this->car_id).' '.$this->driver->getFullName();
			$datetime = time();
			$report = new Report([
				'action_id' => $this::action_id,
				'date_create' => $datetime,
				'description' => $description
			]);
			$report->save();
		}
		parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
	}

	public function delete($order = false)
	{
		if ($order)
		{
			$description = Car::getCarNameById($this->car_id).' '.$this->driver->getFullName();
			$datetime = time();
			$report = new Report([
				'action_id' => $this::action_id_delete,
				'date_create' => $datetime,
				'description' => $description
			]);
			$report->save();
			$order2 = new Order($this->attributes);
			$order2->date_create = time();
			$order2->save();
		}
		return parent::delete(); // TODO: Change the autogenerated stub
	}

	public static function getCarsInPanel()
	{
		$panels = Panel::find()->all();
		$arr = [];
		foreach($panels as $item)
		{
			$arr[$item['car_id']] = Car::getCarNameById($item['car_id']);
		}
		return $arr;
	}

	public function getTrueTariffs()
	{
		$car = Car::findOne(['id' => $this->car_id]);
		$driver = Driver::findOne(['id' => $this->driver_id]);
//		if (($car->use_group_tariff && !$car->group_tariff_id))
//		{
//			throw new HttpException('Для данного авто не определены необходимые групповые тарифы');
//		}
		if ($car->use_group_tariff && $car->group_tariff_id) {
			$tariff = GroupTariff::findOne(['id' => $car->group_tariff_id]);
			$this->town = $tariff->town;
			$this->town_center = $tariff->town_center;
			$this->km_price = $tariff->km_price;
			return true;
		}
		else {
			$tariff = DriverTariff::findOne(['driver_id' => $driver->id]);
			$this->town = $tariff->town;
			$this->town_center = $tariff->town_center;
			$this->km_price = $tariff->km_price;
			return true;
		}
		return false;
	}

	public static function addByCardId($id)
	{
		$model = DriverCar::findOne([
			'mifar_card_id' => $id
		]);
		if (!$model)
			return false;
		else
		{
			$car = $model->getCar()->one();
			$driver = $model->getDriver()->one();
			if (!$car || !$driver)
				return false;
			if (!$tariff = $car->getTariff())
				$tariff = $driver->getTariff();
			if ($tariff)
			{
				$panel = new Panel();
				$panel->car_id = $car->id;
				$panel->driver_id = $driver->id;
				$panel->town = $tariff->town;
				$panel->town_center = $tariff->town_center;
				$panel->km_price = $tariff->km_price;
				$result = $panel->save();
				return $result;
			}
			else
				return false;
		}
	}

	public static function getPlaces()
	{
		return [
			1 => 'Внутренние воздушные линии',
			2 => 'Международные воздушные линии'
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCar()
	{
		return $this->hasOne(Car::className(), ['id' => 'car_id']);
	}

	public function getBrand()
	{
		$brand_id = $this->car->brand_id;
		$model = Brand::findOne(['id' => $brand_id]);
		return $model;
	}

	public function getDriver()
	{
		return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
	}

//	public function beforeSave($insert)
//	{
//		return parent::beforeSave($insert); // TODO: Change the autogenerated stub
//	}
}
