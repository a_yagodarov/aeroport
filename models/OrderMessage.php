<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_message".
 *
 * @property integer $id
 * @property integer $panel_id
 */
class OrderMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['panel_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'panel_id' => 'Panel ID'
        ];
    }
}
