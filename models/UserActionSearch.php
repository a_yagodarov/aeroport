<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserAction;

/**
 * UserActionSearch represents the model behind the search form about `app\models\UserAction`.
 */
class UserActionSearch extends UserAction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'action_id', 'manager_id'], 'integer'],
	          ['date_create', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserAction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	          'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'action_id' => $this->action_id,
            'manager_id' => $this->manager_id,
        ]);
	      if ($this->date_create)
	      {
		      $query->andFilterWhere([
			      'between',
			      'date_create',
			      strtotime($this->date_create),
			      strtotime($this->date_create)+86400
		        ]);
	      }

        return $dataProvider;
    }
}
