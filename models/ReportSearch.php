<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Report;

/**
 * ReportSearch represents the model behind the search form about `app\models\Report`.
 */
class ReportSearch extends Report
{
	public $date_from;
	public $date_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'action_id'], 'integer'],
            [['date_create', 'description', 'date_from', 'date_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Report::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_create' => $this->date_create,
            'action_id' => $this->action_id,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);
	    if ($this->date_from && $this->date_to)
	        $query->andFilterWhere(['between', 'date_create', strtotime($this->date_from), strtotime($this->date_to)]);
		else if ($this->date_from && !$this->date_to)
			$query->andFilterWhere(['>=', 'date_create', strtotime($this->date_from)]);
		else if (!$this->date_from && $this->date_to)
			$query->andFilterWhere(['<=', 'date_create', strtotime($this->date_to)]);
        return $dataProvider;
    }
}
