<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 14.07.2016
 * Time: 20:39
 */

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class PriceWidget extends Widget
{
	public $price;

	public function init()
	{
		parent::init();
		if (is_numeric($this->price)) {
			$this->price .= ' руб.';
		}
	}

	public function run()
	{
		return Html::encode($this->price);
	}
}