<?php

use yii\db\Migration;

/**
 * Handles the creation for table `order_message_table`.
 */
class m160709_104104_create_order_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_message', [
            'id' => $this->primaryKey(),
	        'panel_id' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_message');
    }
}
