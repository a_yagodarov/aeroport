<?php

use yii\db\Migration;

class m161114_105640_add_user_action_date_create_field extends Migration
{
    public function up()
    {
			$this->addColumn(\app\models\UserAction::tableName(), 'date_create', $this->integer());
    }

    public function down()
    {
	    $this->dropColumn(\app\models\UserAction::tableName(), 'date_create');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
