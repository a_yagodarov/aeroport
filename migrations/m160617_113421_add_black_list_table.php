<?php

use yii\db\Migration;

class m160617_113421_add_black_list_table extends Migration
{
    public function up()
    {
		$this->createTable('car_black_list', [
			'id' => $this->primaryKey(),
			'image' => $this->string(64),
			'video' => $this->string(64)
		]);
	    $this->addColumn('car', 'is_black_list', $this->smallInteger().' DEFAULT 0');
    }

    public function down()
    {
        $this->dropTable('car_black_list');
	    $this->dropColumn('car', 'is_black_list');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
