<?php

use yii\db\Migration;

/**
 * Handles the creation for table `brand_name`.
 */
class m160628_161121_create_brand_name extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('brand', [
            'id' => $this->primaryKey(),
	        'image' => $this->string(64),
	        'name' => $this->string(32)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('brand');
    }
}
