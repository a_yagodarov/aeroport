<?php

use yii\db\Migration;

class m161115_090452_add_text_column_into_user_action extends Migration
{
    public function up()
    {
			$this->addColumn(\app\models\UserAction::tableName(), 'text', $this->string(64));
    }

    public function down()
    {
	    $this->dropColumn(\app\models\UserAction::tableName(), 'text');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
