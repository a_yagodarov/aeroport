<?php

use yii\db\Migration;

class m161117_130432_change_report_description_length_1000 extends Migration
{
    public function up()
    {
		$this->alterColumn(\app\models\Report::tableName(), 'description', $this->string(1000));
    }

    public function down()
    {
        echo "m161117_130432_change_report_description_length_1000 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
