<?php

use yii\db\Migration;

class m161113_085317_add_type_client_foreign_key_car extends Migration
{
    public function up()
    {
	    $this->addColumn(\app\models\Car::tableName(), 'client_type_id', $this->integer());
			$this->addForeignKey('car_type_client_id', \app\models\Car::tableName(), 'client_type_id', \app\models\TypeClient::tableName(), 'id');
    }

    public function down()
    {
	      $this->execute("SET foreign_key_checks = 0;");
        $this->dropColumn(\app\models\Car::tableName(), 'client_type_id');
	      $this->dropForeignKey('car_type_client_id', \app\models\Car::tableName());
	      $this->execute("SET foreign_key_checks = 1;");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
