<?php

use yii\db\Migration;

class m161112_161441_change_rating_default extends Migration
{
    public function up()
    {
			$this->alterColumn(\app\models\Driver::tableName(), 'rating', $this->integer().' DEFAULT 5 NOT NULL');
	    $this->execute("UPDATE ".\app\models\Driver::tableName()." SET rating = 5");
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
