<?php

use yii\db\Migration;

class m161114_171927_add_car_fields extends Migration
{
    public function up()
    {
			$this->addColumn(\app\models\Car::tableName(), 'bank_card_payment', $this->smallInteger().' DEFAULT 0 NULL');
			$this->addColumn(\app\models\Car::tableName(), 'report_documents', $this->smallInteger().' DEFAULT 0 NULL');
    }

    public function down()
    {
	    $this->dropColumn(\app\models\Car::tableName(), 'bank_card_payment');
	    $this->dropColumn(\app\models\Car::tableName(), 'report_documents');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
