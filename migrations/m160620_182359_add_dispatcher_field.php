<?php

use yii\db\Migration;

class m160620_182359_add_dispatcher_field extends Migration
{
    public function up()
    {
		$this->insert(\app\models\Role::tableName(), [
			'id' => 2,
			'name' => 'dispatcher',
			'title' => 'Оператор въезда'
		]);
    }

    public function down()
    {
        echo "m160620_182359_add_dispatcher_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
