<?php

use yii\db\Migration;

class m160816_073355_add_action_field extends Migration
{
    public function up()
    {
		$this->insert('{{%action}}', [
			'id' => 6,
			'name' => 'Выезд'
		]);
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
