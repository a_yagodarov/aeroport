<?php

use yii\db\Migration;

class m161118_125511_tariff_town_length_256 extends Migration
{
    public function up()
    {
		$this->alterColumn(\app\models\GroupTariff::tableName(), 'town', $this->string(256));
		$this->alterColumn(\app\models\DriverTariff::tableName(), 'town', $this->string(256));
    }

    public function down()
    {
        echo "m161118_125511_tariff_town_length_256 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
