<?php

use yii\db\Migration;

class m160701_072207_add_driver_fields extends Migration
{
    public function up()
    {
	    $this->addColumn(\app\models\Driver::tableName(), 'photo_image', $this->string(32));
		$this->addColumn(\app\models\Driver::tableName(), 'passport_number', $this->string(32));
		$this->addColumn(\app\models\Driver::tableName(), 'driver_card_serial_number', $this->string(32));
		$this->addColumn(\app\models\Driver::tableName(), 'reviews_text', $this->string(256));
		$this->addColumn(\app\models\Driver::tableName(), 'mvd_review_data', $this->string(256));
    }

    public function down()
    {
	    $this->dropColumn(\app\models\Driver::tableName(), 'photo_image');
	    $this->dropColumn(\app\models\Driver::tableName(), 'passport_number');
	    $this->dropColumn(\app\models\Driver::tableName(), 'driver_card_serial_number');
	    $this->dropColumn(\app\models\Driver::tableName(), 'reviews_text');
	    $this->dropColumn(\app\models\Driver::tableName(), 'mvd_review_data');
    }
}
