<?php

use yii\db\Migration;

class m170225_151349_null_tariff extends Migration
{
    public function up()
    {
        $this->alterColumn(\app\models\DriverTariff::tableName(), 'use_single_tariff', $this->smallInteger()->null());
        $this->alterColumn(\app\models\Driver::tableName(), 'rating', $this->integer()->defaultValue(5));
    }

    public function down()
    {
        $this->alterColumn(\app\models\DriverTariff::tableName(), 'use_single_tariff', $this->smallInteger()->notNull());
        $this->alterColumn(\app\models\Driver::tableName(), 'rating', $this->integer()->defaultValue(0));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
