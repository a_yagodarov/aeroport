<?php

use yii\db\Migration;

class m160710_082628_add_manager_terminal_field extends Migration
{
//    public function up()
//    {
//		$this->addColumn(\app\models\User::tableName(), 'terminal_id', $this->integer());
//	    Yii::$app->db->createCommand("alter table ".\app\models\Terminal::tableName()." engine = InnoDB;")->execute();
//	    $this->addForeignKey(
//		    'user_terminal_id',
//		    \app\models\User::tableName(),
//		    'terminal_id',
//		    \app\models\Terminal::tableName(),
//		    'id',
//		    'SET NULL',
//		    'SET NULL'
//	    );
//	    $this->insert(\app\models\Terminal::tableName(), [
//		    'name' => 'Внутренние воздушные линии',
//	    ]);
//	    $this->insert(\app\models\Terminal::tableName(), [
//		    'name' => 'Международные воздушные линии',
//	    ]);
//    }
//
//    public function down()
//    {
//	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
//	    $this->dropForeignKey('user_terminal_id', \app\models\User::tableName());
//	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
//        $this->dropColumn(\app\models\User::tableName(), 'terminal_id');
//	    $this->truncateTable(\app\models\Terminal::tableName());
//    }

    public function safeUp()
    {
        $this->addColumn(\app\models\User::tableName(), 'terminal_id', $this->integer());
	    Yii::$app->db->createCommand("alter table ".\app\models\Terminal::tableName()." engine = InnoDB;")->execute();
	    $this->addForeignKey(
		    'user_terminal_id',
		    \app\models\User::tableName(),
		    'terminal_id',
		    \app\models\Terminal::tableName(),
		    'id',
		    'SET NULL',
		    'SET NULL'
	    );
	    $this->insert(\app\models\Terminal::tableName(), [
		    'name' => 'Внутренние воздушные линии',
	    ]);
	    $this->insert(\app\models\Terminal::tableName(), [
		    'name' => 'Международные воздушные линии',
	    ]);
    }

    public function safeDown()
    {
	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
	    $this->dropForeignKey('user_terminal_id', \app\models\User::tableName());
	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
	    $this->dropColumn(\app\models\User::tableName(), 'terminal_id');
	    $this->truncateTable(\app\models\Terminal::tableName());
    }
}
