<?php

use yii\db\Migration;

class m161111_140915_change_messages_managers extends Migration
{
    public function up()
    {
        $this->update(\app\models\Terminal::tableName(), ["name" => "ВВЛ"], ["id" => 1]);
        $this->update(\app\models\Terminal::tableName(), ["name" => "МВЛ"], ["id" => 2]);
    }

    public function down()
    {
        echo "m161111_140915_change_messages_managers cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
