<?php

use yii\db\Migration;

class m160718_190641_add_order_manager_id extends Migration
{
    public function up()
    {
		$this->addColumn(\app\models\Order::tableName(), 'user_id', $this->integer());
    }

    public function down()
    {
	    $this->dropColumn(\app\models\Order::tableName(), 'user_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
