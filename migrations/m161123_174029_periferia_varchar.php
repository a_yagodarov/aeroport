<?php

use yii\db\Migration;

class m161123_174029_periferia_varchar extends Migration
{
    public function up()
    {
		$this->alterColumn(\app\models\Panel::tableName(), 'periferia', $this->string(64));
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
