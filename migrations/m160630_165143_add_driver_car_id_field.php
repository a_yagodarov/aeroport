<?php

use yii\db\Migration;

class m160630_165143_add_driver_car_id_field extends Migration
{
    public function up()
    {
		$this->addColumn(\app\models\DriverCar::tableName(), 'mifar_card_id', $this->string(64));
	    $this->alterColumn(\app\models\Car::tableName(), 'use_group_tariff', $this->integer().' DEFAULT 0 NOT NULL');
    }

    public function down()
    {
	    $this->dropColumn(\app\models\DriverCar::tableName(), 'mifar_card_id');
	    $this->alterColumn(\app\models\Car::tableName(), 'use_group_tariff', $this->integer().' DEFAULT 0 NOT NULL');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
