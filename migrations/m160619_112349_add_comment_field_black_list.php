<?php

use yii\db\Migration;

class m160619_112349_add_comment_field_black_list extends Migration
{
    public function up()
    {
		$this->addColumn(\app\models\CarBlackList::tableName(), 'comment', $this->string(255));
    }

    public function down()
    {
	    $this->dropColumn(\app\models\CarBlackList::tableName(), 'comment');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
