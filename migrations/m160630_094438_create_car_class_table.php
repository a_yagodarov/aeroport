<?php

use yii\db\Migration;

/**
 * Handles the creation for table `car_class_table`.
 */
class m160630_094438_create_car_class_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('car_class', [
              'id' => $this->primaryKey(),
              'name' => $this->string(4)
        ]);
	    $this->alterColumn(\app\models\Car::tableName(), 'class', $this->integer());
	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
	    $this->addForeignKey('car_class_id', \app\models\Car::tableName(), 'class', 'car_class', 'id', 'SET NULL');
	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
	    $this->dropForeignKey('car_class_id', \app\models\Car::tableName());
	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
	    $this->dropTable('car_class');
    }
}
