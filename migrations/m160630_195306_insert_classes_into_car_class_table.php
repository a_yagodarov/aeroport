<?php

use yii\db\Migration;

class m160630_195306_insert_classes_into_car_class_table extends Migration
{
    public function up()
    {
	    $sql = "INSERT INTO `car_class`(`name`) VALUES ('A'),('B'),('C'),('D'),('E'),('F'),('MPV'),('SUV'),('CUV'),('mBUS')";
		Yii::$app->db->createCommand($sql)->execute();
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
