<?php

use yii\db\Migration;

class m161122_211647_add_order_single_tariff extends Migration
{
	public function up()
	{
		$this->addColumn(\app\models\Order::tableName(), 'use_single_tariff', $this->boolean());
	}

	public function down()
	{
		$this->dropColumn(\app\models\Order::tableName(), 'use_single_tariff');
	}

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
