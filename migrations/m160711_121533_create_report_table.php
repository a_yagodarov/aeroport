<?php

use yii\db\Migration;

/**
 * Handles the creation for table `report_table`.
 */
class m160711_121533_create_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
	    $this->createTable('action', [
		    'id' => $this->primaryKey(),
		    'name' => $this->string(32)
	    ],
		    'ENGINE InnoDB');

	    $this->insert('action', [
		    'id' => 1,
		    'name' => 'Новое авто'
	    ]);
	    $this->insert('action', [
		    'id' => 2,
		    'name' => 'Новый водитель'
	    ]);
	    $this->insert('action', [
		    'id' => 3,
		    'name' => 'Новый в черном списке'
	    ]);
	    $this->insert('action', [
		    'id' => 4,
		    'name' => 'Въезд'
	    ]);
	    $this->insert('action', [
		    'id' => 5,
		    'name' => 'Заказ'
	    ]);

        $this->createTable('report', [
            'id' => $this->primaryKey(),
	        'date_create' => $this->integer(),
	        'description' => $this->string(64),
	        'action_id' => $this->integer()
        ],
	        'ENGINE InnoDB');

	    $this->addForeignKey('report_action_id', 'report', 'action_id', 'action', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
	    $this->dropForeignKey('report_action_id', 'report');
	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
        $this->dropTable('action');
        $this->dropTable('report');
    }
}
