<?php

use yii\db\Migration;

class m161121_205834_add_panel_use_single_tariff extends Migration
{
    public function up()
    {
		$this->addColumn(\app\models\Panel::tableName(), 'use_single_tariff', $this->boolean());
    }

    public function down()
    {
	    $this->dropColumn(\app\models\Panel::tableName(), 'use_single_tariff');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
