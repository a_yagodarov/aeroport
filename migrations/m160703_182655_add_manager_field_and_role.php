<?php

use yii\db\Migration;

class m160703_182655_add_manager_field_and_role extends Migration
{
    public function up()
    {
		$this->insert(\app\models\Role::tableName(), [
			'name' => 'manager',
			'title' => 'Менеджер'
		]);
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
