<?php

use yii\db\Migration;

/**
 * Handles the creation for table `driver_tariff_table`.
 */
class m160701_120503_create_driver_tariff_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('driver_tariff', [
            'id' => $this->primaryKey(),
	        'driver_id' => $this->integer(),
	        'town' => $this->string(64),
	        'town_center' => $this->string(64),
	        'km_price' => $this->string(64)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('driver_tariff');
    }
}
