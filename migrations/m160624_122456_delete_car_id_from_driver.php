<?php

use yii\db\Migration;

class m160624_122456_delete_car_id_from_driver extends Migration
{
    public function up()
    {
	    $this->dropForeignKey('driver_car_id', \app\models\Driver::tableName());
		$this->dropColumn(\app\models\Driver::tableName(), 'car_id');
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
