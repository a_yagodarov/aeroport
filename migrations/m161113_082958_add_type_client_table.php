<?php

use yii\db\Migration;

class m161113_082958_add_type_client_table extends Migration
{
    public function up()
    {
			$this->createTable('type_client', [
				'id' => $this->primaryKey(),
				'name' => $this->string(64)
			],
				'ENGINE InnoDB');
    }

    public function down()
    {
        $this->dropTable('type_client');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
