<?php

use yii\db\Migration;

class m161121_003751_add_single_tariff_field extends Migration
{
    public function up()
    {
		$this->addColumn(\app\models\DriverTariff::tableName(), 'use_single_tariff', $this->boolean().' DEFAULT 0 NOT NULL');
    }

    public function down()
    {
	    $this->dropColumn(\app\models\DriverTariff::tableName(), 'use_single_tariff');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
