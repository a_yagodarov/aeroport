<?php

use yii\db\Migration;

class m161121_150918_periferia_varchar_64 extends Migration
{
    public function up()
    {
		$this->alterColumn(\app\models\GroupTariff::tableName(), 'periferia', $this->string(64));
		$this->alterColumn(\app\models\DriverTariff::tableName(), 'periferia', $this->string(64));
    }

    public function down()
    {
	    $this->dropColumn(\app\models\GroupTariff::tableName(), 'periferia');
	    $this->dropColumn(\app\models\DriverTariff::tableName(), 'periferia');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
