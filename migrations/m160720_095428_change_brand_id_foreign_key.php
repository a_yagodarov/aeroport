<?php

use yii\db\Migration;

class m160720_095428_change_brand_id_foreign_key extends Migration
{
    public function up()
    {
		$this->dropForeignKey('brand_id_brand_id', \app\models\Car::tableName());
		$this->addForeignKey('brand_id_brand_id', \app\models\Car::tableName(), 'brand_id', \app\models\Brand::tableName(), 'id', 'SET NULL', 'NO ACTION');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
