<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_action`.
 */
class m161114_101613_create_user_action_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_action', [
            'id' => $this->primaryKey(),
	          'action_id' => $this->integer(),
	          'manager_id' => $this->integer()
        ]);
	      $this->addForeignKey('user_action_manager_id', 'user_action', 'manager_id', \app\models\User::tableName(), 'id', 'CASCADE', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_action');
    }
}
