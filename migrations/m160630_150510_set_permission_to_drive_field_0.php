<?php

use yii\db\Migration;

class m160630_150510_set_permission_to_drive_field_0 extends Migration
{
    public function up()
    {
		$this->alterColumn(\app\models\Car::tableName(), 'permission_to_drive', $this->integer().' DEFAULT 0');
    }

    public function down()
    {
	    $this->alterColumn(\app\models\Car::tableName(), 'permission_to_drive', $this->integer().' DEFAULT 1');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
