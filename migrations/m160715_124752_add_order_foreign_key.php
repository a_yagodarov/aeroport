<?php

use yii\db\Migration;

class m160715_124752_add_order_foreign_key extends Migration
{
    public function up()
    {
	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
		$this->addForeignKey('panel_car_id', \app\models\Panel::tableName(), 'car_id', \app\models\Car::tableName(), 'id', 'CASCADE');
		$this->addForeignKey('panel_driver_id', \app\models\Panel::tableName(), 'driver_id', \app\models\Driver::tableName(), 'id', 'CASCADE');
		$this->addForeignKey('order_car_id', \app\models\Order::tableName(), 'car_id', \app\models\Car::tableName(), 'id', 'CASCADE');
		$this->addForeignKey('order_driver_id', \app\models\Order::tableName(), 'driver_id', \app\models\Driver::tableName(), 'id', 'CASCADE');
	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
    }

    public function down()
    {
        echo "m160715_124752_add_order_foreign_key cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
