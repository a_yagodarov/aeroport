<?php

use yii\db\Migration;

class m160630_074714_add_car_fields extends Migration
{
    public function up()
    {
		$this->addColumn(\app\models\Car::tableName(), 'brand_id', $this->integer());
		$this->addColumn(\app\models\Car::tableName(), 'external_baggage', $this->integer());
		$this->addColumn(\app\models\Car::tableName(), 'license', $this->integer());
		$this->addColumn(\app\models\Car::tableName(), 'license_requisites', $this->string());
		$this->addColumn(\app\models\Car::tableName(), 'use_group_tariff', $this->integer());
		$this->addColumn(\app\models\Car::tableName(), 'group_tariff_id', $this->integer().' DEFAULT NULL NULL');
	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
	    $this->addForeignKey('brand_id_brand_id', \app\models\Car::tableName(), 'brand_id', \app\models\Brand::tableName(), 'id');
	    $this->addForeignKey('group_tariff_id_tariff_id', \app\models\Car::tableName(), 'group_tariff_id', \app\models\GroupTariff::tableName(), 'id');
	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
    }

    public function down()
    {
	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
	    $this->dropForeignKey('brand_id_brand_id', \app\models\Car::tableName());
	    $this->dropForeignKey('group_tariff_id_tariff_id', \app\models\Car::tableName());
	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
	    $this->dropColumn(\app\models\Car::tableName(), 'brand_id');
	    $this->dropColumn(\app\models\Car::tableName(), 'external_baggage');
	    $this->dropColumn(\app\models\Car::tableName(), 'license');
	    $this->dropColumn(\app\models\Car::tableName(), 'license_requisites');
	    $this->dropColumn(\app\models\Car::tableName(), 'use_group_tariff');
	    $this->dropColumn(\app\models\Car::tableName(), 'group_tariff_id');
    }
}
