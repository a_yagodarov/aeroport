<?php

use yii\db\Migration;

/**
 * Handles the creation for table `orders`.
 */
class m160714_175805_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
//	    $data = Yii::$app->db->getTableSchema(\app\models\Panel::tableName());
	    Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
	    $this->dropTable('order');
	    Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
        Yii::$app->db->createCommand("create table `order` like `panel`")->execute();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order');
    }
}
