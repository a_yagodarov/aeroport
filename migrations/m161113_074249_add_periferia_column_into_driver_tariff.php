<?php

use yii\db\Migration;

class m161113_074249_add_periferia_column_into_driver_tariff extends Migration
{
    public function up()
    {
			$this->addColumn(\app\models\DriverTariff::tableName(), 'periferia', $this->integer());
			$this->addColumn(\app\models\GroupTariff::tableName(), 'periferia', $this->integer());
			$this->addColumn(\app\models\Panel::tableName(), 'periferia', $this->integer());
			$this->addColumn(\app\models\Order::tableName(), 'periferia', $this->integer());
			$this->addColumn(\app\models\CarTariff::tableName(), 'periferia', $this->integer());
    }

    public function down()
    {
	    $this->dropColumn(\app\models\DriverTariff::tableName(), 'periferia');
	    $this->dropColumn(\app\models\GroupTariff::tableName(), 'periferia');
	    $this->dropColumn(\app\models\Panel::tableName(), 'periferia');
	    $this->dropColumn(\app\models\Order::tableName(), 'periferia');
	    $this->dropColumn(\app\models\CarTariff::tableName(), 'periferia');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
