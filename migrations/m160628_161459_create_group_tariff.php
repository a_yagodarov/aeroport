<?php

use yii\db\Migration;

/**
 * Handles the creation for table `group_tariff`.
 */
class m160628_161459_create_group_tariff extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group_tariff', [
            'id' => $this->primaryKey(),
	        'name' => $this->string(),
	        'town_center' => $this->string(64),
	        'town' => $this->string(64),
	        'km_price' => $this->string(64),
	        'comment' => $this->string(256)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('group_tariff');
    }
}
