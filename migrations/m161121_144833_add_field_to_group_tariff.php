<?php

use yii\db\Migration;

class m161121_144833_add_field_to_group_tariff extends Migration
{
    public function up()
    {
		$this->addColumn(\app\models\GroupTariff::tableName(), 'use_single_tariff', $this->boolean().' DEFAULT 0 NOT NULL');
    }

    public function down()
    {
	    $this->dropColumn(\app\models\GroupTariff::tableName(), 'use_single_tariff');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
