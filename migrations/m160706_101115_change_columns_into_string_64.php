<?php

use yii\db\Migration;

class m160706_101115_change_columns_into_string_64 extends Migration
{
    public function up()
    {
		$this->alterColumn(\app\models\Panel::tableName(), 'town', $this->string(64));
		$this->alterColumn(\app\models\Panel::tableName(), 'town_center', $this->string(64));
		$this->alterColumn(\app\models\Panel::tableName(), 'km_price', $this->string(64));
    }

    public function down()
    {
        echo "m160706_101115_change_columns_into_string_64 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
