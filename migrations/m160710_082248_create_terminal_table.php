<?php

use yii\db\Migration;

/**
 * Handles the creation for table `terminal_table`.
 */
class m160710_082248_create_terminal_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('terminal', [
            'id' => $this->primaryKey(),
	        'name' => $this->string(128)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('terminal');
    }
}
