<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TypeClient */

$this->title = 'Обновить тип клиента: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы клиентов', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="type-client-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
