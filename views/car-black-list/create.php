<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CarBlackList */

$this->title = 'Внести в черный список';
//$this->params['breadcrumbs'][] = ['label' => 'Car Black Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-black-list-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
