<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CarBlackList */

$this->title = 'Update Car Black List: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Car Black Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="car-black-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
