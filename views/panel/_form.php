<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Panel */
/* @var $form yii\widgets\ActiveForm */
$urlGetDrivers = \yii\helpers\Url::to('/driver/get-drop-down-drivers');
$urlGetTariff = \yii\helpers\Url::to('/panel/get-cars-tariff');
$urlGetDriverTariff = \yii\helpers\Url::to('/driver/get-drivers-tariff');
$script = <<< JS
var data;
function replaceCarsDropDown(selector)
{
	$.ajax({
		url : '$urlGetDrivers',
		type : 'post',
		data : {
			id : $('[name="Panel[car_id]"]').val(),
			//order_id: $model->id
		},
		success : function(data){
			$(selector).empty();
			$(selector).append($(data).children());
			$(selector).val('')
		}
	})
}

function getTariffPrice()
{
	$.ajax({
		url : '$urlGetTariff',
		type : 'post',
		data : {
			id : $('[name="Panel[car_id]"]').val(),
		},
		success : function(data){
			data = JSON.parse(data);
			console.log(data);
			if ("name" in data)
			{
				$('#panel-town').val(data['town']);
				$('#panel-town_center').val(data['town_center']);
				$('#panel-periferia').val(data['periferia']);
				$('#panel-km_price').val(data['km_price']);
				$('#panel-use_single_tariff').prop('checked', data['use_single_tariff']);
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('disabled', true);
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('readonly', true);
				$('#w0 > div.panel.panel-default input').attr('disabled', true);
				return true;
			}
			else
			{
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('readonly', false);
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('disabled', false);
				$('#w0 > div.panel.panel-default input').attr('disabled', false);

				$('#panel-town').val(data['town']);
				$('#panel-town_center').val(data['town_center']);
				$('#panel-periferia').val(data['periferia']);
				$('#panel-km_price').val(data['km_price']);
				$('#panel-use_single_tariff').prop('checked', 0);
			}
		}
	});
}
function addDriverChangeGetTariff()
{
	$('[name="Panel[driver_id]"').on('change', function(){
		if	($('#panel-town').val() == $('#panel-town_center').val() ==	$('#panel-km_price').val() == "")
		{
			$.ajax({
				url : '$urlGetDriverTariff',
				type : 'post',
				data : {
					id : $('[name="Panel[driver_id]"]').val(),
				},
				success : function(data){
					data = JSON.parse(data);
					console.log(data);
					var attr = $('#panel-use_new_tariffs').attr('disabled');
					//console.log('attr = ' + attr);
					if (attr !== undefined){
							console.log('disabled')
						}
					else{
							$('#panel-town').val(data['town']);
							$('#panel-town_center').val(data['town_center']);
							$('#panel-periferia').val(data['periferia']);
							$('#panel-km_price').val(data['km_price']);
							$('#panel-use_single_tariff').prop('checked', data['use_single_tariff']);
						}
				}
			});
		}
	})
}
$('[name="Panel[car_id]"]').on('change', function(){
	console.log(this+$(this).val());
	$('[name="Panel[car_id]"]').val($(this).val());
	replaceCarsDropDown('#panel-driver_id');
	getTariffPrice();
	addDriverChangeGetTariff()
});
replaceCarsDropDown('#panel-driver_id');
//disableIfGroupTariff();

if ('$model->isNewRecord' == 1)
	getTariffPrice();
	console.log('$model->isNewRecord');
//$('.main-form').removeClass('hidden');
$('#select2-panel_car_value_select2-container').focus();
JS;


$this->registerJs($script, \yii\web\View::POS_END);
?>

<div class="panel-form main-form">
<?// \yii\widgets\Pjax::begin([
//	'id' => 'w0'
//])?>
    <?php $form = ActiveForm::begin([
	    'enableAjaxValidation' => true,
	    'enableClientValidation' => false,
    ]); ?>

	<?= $form->field($model, 'car_id')->widget(Select2::className(), [
		'data' => \app\models\Car::getCarsForDropDownList([
			'panel' => true,
		]),
		'options' => [
//			'id' => 'panel_car_value_select2',
			'placeholder' => 'Выберите авто ...',
			'enableAjaxValidation' => true
		],
		'pluginOptions' => [
			'allowClear' => true
		]
	]) ?>

	<?= $form->field($model, 'driver_id')->dropDownList([],
		[
			'prompt' => '-- Выберите --'
		])	?>

<!--	--><?//= $form->field($model, 'car_id')->dropDownList([], ['prompt' => 'Выберите авто'])	?>

	<?= $form->field($model, 'station_time')->textInput() ?>

	<?= $form->field($model, 'use_new_tariffs')->checkbox() ?>

	<div class="panel panel-default">
		<div class="panel-heading">
			Тарифы
		</div>
		<div class="panel-body">
			<?= $form->field($model, 'town_center')->textInput() ?>

			<?= $form->field($model, 'town')->textInput() ?>

			<?= $form->field($model, 'use_single_tariff')->checkbox() ?>

			<?= $form->field($model, 'periferia')->textInput() ?>

			<?= $form->field($model, 'km_price')->textInput() ?>
		</div>
	</div>

<!--	<sup>* Данные полей серого цвета не  </sup>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Применить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?// \yii\widgets\Pjax::end() ?>
</div>
