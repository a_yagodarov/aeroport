<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\assets\AppAsset2;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PanelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'возле терминала стоит машин такси: '.$dataProvider->count;
$this->params['breadcrumbs'][] = $this->title;
AppAsset2::register($this);

$urlProfile = \yii\helpers\Url::to('/panel/profile?id=');
$urlAjax = \yii\helpers\Url::toRoute(['/panel/order']);
$urlDelete = \yii\helpers\Url::toRoute('/panel/delete');
$urlPrint = \yii\helpers\Url::to('/panel/print');
$js = <<< JS
var modal = document.getElementById('profile');
var blur = false;
var id = '';
function orderAndPrint()
{
	$('.button').click(function(){
		if (confirm('Вы уверены что хотите сделать заказ?'))
		{
			zone = $(this).attr('zone_id');
			/*
			Заказываем такси и удаляем запись
			 */
			$.ajax({
				url : '$urlAjax'+'?id='+id+'&zone='+zone,
				success : function(){

				}
			});
			/*
			Открываем окно с печатью
			 */
			url='$urlPrint'+'?id='+id+'&zone='+zone;
			win = window.open(url, '_blank');
			/*
			Перенаправляем текущую страницу
			  */
			window.location.replace('$urlDelete'+'?id='+id);
			win.focus();
			win.print();
		}
	});
}
function pasteProfile(panel)
{
	id = panel['panel_id'];
	$('#car_name').html(panel['car_brand_name']+' '+panel['model']+' '+panel['number']);
	$('#name').html(panel['name']);
	$('#surname').html(panel['surname']);
	$('#middle_name').html(panel['middle_name']);
	$('#experience').html('Стаж за рулем - '+panel['experience']);
	$('#driver_image').attr('src', '/web/files/images/drivers/'+panel['photo_image']);

	$('#car_inside_image').attr('src', '/web/files/images/'+panel['car_id']+'/'+panel['car_inside_image']);
	$('#car_outside_image').attr('src', '/web/files/images/'+panel['image']);

	$('#car_class').html(panel['car_class']);
	$('#car_number').html(panel['number']);
	$('#car_year').html(panel['year']);
	$('#car_passengers').html(panel['number_of_passengers']);
	$('#car_year').html(panel['year']);

	$('#car_baggage').html(panel['external_baggage'] == true ? 'Да' : 'Нет');
	$('#car_bank_card_payment').html(panel['bank_card_payment'] == true ? 'Да' : 'Нет');
	$('#car_documents').html(panel['report_documents'] == true ? 'Да' : 'Нет');
	$('#car_baby_chair').html(panel['baby_chair'] == true ? 'Да' : 'Нет');
	stars = panel['rating'];
	starsYellow = '';
	i = stars;
	while (i > 0)
	{
		if (i != 1)
			starsYellow += '&#9733&nbsp'
		else
			starsYellow += '&#9733'
		i--;
	}
	i = 5 - stars;
	starsWhite = '';
	while (i > 0)
	{
		starsWhite += '&#9733&nbsp';
		i--;
	}
	$('#stars-yellow').html(starsYellow);
	$('#stars-white').html(starsWhite);

	if (panel['use_single_tariff'] == "1")
	{
		console.log('1');
		$('#town').html(panel['town']);
		$('#town').show();
		$('#town_center').hide();
		$('#periferia').hide();
		$('#km_price').hide();
	}
	else
	{
		console.log('0');
		if (panel['town'])
		{
			$('#town').html('Уфа - '+panel['town']);
			$('#town').show();
		}
		else
		{
			$('#town').hide();
		}

		if (panel['town_center'])
		{
			$('#town_center').html('Уфа(центр) - '+panel['town_center']);
			$('#town_center').show();
		}
		else
		{
			$('#town_center').hide();
		}

		if (panel['periferia'])
		{
			$('#periferia').html('Периферия - '+panel['periferia']);
			$('#periferia').show();
		}
		else
		{
			$('#periferia').hide();
		}

		if (panel['km_price'])
		{
			$('#km_price').html('Цена за км - '+panel['km_price']);
			$('#km_price').show();
		}
		else
		{
			$('#km_price').hide();
		}
	}
	if (panel['reviews_text'])
		$('#review').html(panel['reviews_text']);
	else
		$('#review').html('Ничего не найдено');

}
$('.table tr').click(function(){
	if (blur == false)
	{
		item = $(this);
		$.ajax({
			url : '$urlProfile'+$(this).attr('item_id'),
			success : function(data){
				$('.panel-index').addClass('blur');
				blur = true;
				modal.style.display= 'block';
				panel= JSON.parse(data);
				pasteProfile(panel);
				console.log(panel);
			}
		});
	}
});
$('.close').click(function(){
    $('.panel-index').removeClass('blur');
    $('.modal_ask').fadeOut(1);
    blur = false;
    modal.style.display = "none";
})
$('.panel-index').click(function(){
    $('.panel-index').removeClass('blur');
    $('.modal_ask').fadeOut(1);
    blur = false;
    modal.style.display = "none";
});
$('.book').click(function(){
	$('.modal_ask').fadeIn(1);
});
$('.nope').click(function(){
	$('.modal_ask').fadeOut(1);
});
$('table tbody tr').hover(

	function()
	{
		$(this).addClass('itemfocus')
	},

	function()
	{
		$(this).removeClass('itemfocus')
	}
)
orderAndPrint();
JS;

$this->registerJs($js);
$models = $dataProvider->getModels();
usort($models, function($a, $b){
	if ($a->driver->rating == $b->driver->rating) {
		if ($a->town == $b->town) {
			return 0;
		}
		return ($a->town < $b->town) ? -1 : 1;
	}
	return ($a->driver->rating > $b->driver->rating) ? -1 : 1;
});
?>
<div class="panel-index main">

<h1 class="title"><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<div>
    <p>
        <?= Html::a('Въезд авто', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('Выезд авто', ['delete2'], ['class' => 'btn btn-warning']) ?>
	</p>
</div>

<table class="table">
	<tbody>
		<?
		foreach ($models as $model) {
			echo '<tr item_id="'.$model->id.'">';
			echo Html::tag('td', Html::img($model->car->getImagePath(), ['width' => '200px']));
			echo Html::tag('td', '<b>'.$model->car->getCarBrand().' '.$model->car->model.'</b>');
			if ($model->use_single_tariff)
			{
				echo Html::tag('td', $model->town, [
					'colspan' => 4
				]);
				$content = '';
				if ($model->car->bank_card_payment) $content .= '<span class="span_point" style="color: green;">&bull;</span>';
				if ($model->car->baby_chair) $content .= '<span class="span_point" style="color: blue;">&bull;</span>';
				if ($model->car->external_baggage) $content .= '<span class="span_point" style="color: yellow;">&bull;</span>';
				if ($model->car->report_documents) $content .= '<span class="span_point" style="color: red;">&bull;</span>';
				echo Html::tag('td', $content);
			}
			else
			{
				echo Html::tag('td', $model->town_center);
				echo Html::tag('td', $model->town);
				echo Html::tag('td', $model->periferia ? $model->periferia : '-');
				echo Html::tag('td', $model->km_price);
				$content = '';
				if ($model->car->bank_card_payment) $content .= '<span class="span_point" style="color: green;">&bull;</span>';
				if ($model->car->baby_chair) $content .= '<span class="span_point" style="color: blue;">&bull;</span>';
				if ($model->car->external_baggage) $content .= '<span class="span_point" style="color: yellow;">&bull;</span>';
				if ($model->car->report_documents) $content .= '<span class="span_point" style="color: red;">&bull;</span>';
				echo Html::tag('td', $content);
			}
			echo '</tr>';
		}
		?>
	</tbody>
</table>
</div>
<div id="profile" class="modal">

	<!-- Modal content -->
	<div class="model-header">
		<div id="car_name"></div><div class="close">&times;</div>
	</div>
	<div class="modal-content">
		<div class="col-xs-12">
			<div class="col-xs-5">
				<div class="info">Информация о водителе</div>
				<br>
				<div class="row">
					<div class="col-xs-6">
						<img id="driver_image" src="/web/files/images/drivers/kX_opju7f.jpg">
					</div>
					<div class="col-xs-6 lines">
						<div id="surname" class="name"></div>
						<div id="name" class="name"></div>
						<div id="middle_name" class="name"></div>
						<div id="experience"></div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-6">
						<div class="book">Книга отзывов</div>
					</div>
					<div class="col-xs-6 stars">
						<span class="stars-yellow" id="stars-yellow">

						</span>
						<span class="stars-white" id="stars-white">

						</span>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-6">
						<img id="car_outside_image" src="">
					</div>
					<div class="col-xs-6">
						<img id="car_inside_image" src="">
					</div>
				</div>
			</div>
			<div class="col-xs-7" style="padding-left: 30px;">
				<div class="info">Информация об автомобиле</div>
				<br>
				<div class="row">
					<div class="col-xs-6 lines">
						<div>Класс автомобиля </div>
						<div>Государственный номер</div>
						<div>Год выпуска т.с</div>
						<div>Количество мест </div>
						<div>Доп.багажник на крыше</div>
						<div>Наличие детского кресла</div>
						<div>Оплата банковскими картами</div>
						<div>Отчетные документы</div>
					</div>
					<div class="col-xs-6 lines">
						<div id="car_class">C</div>
						<div id="car_number">Х777ХХ77</div>
						<div id="car_year">2014</div>
						<div id="car_passengers">4</div>
						<div id="car_baggage">Да</div>
						<div id="car_baby_chair">Да</div>
						<div id="car_bank_card_payment">Да</div>
						<div id="car_documents">Да</div>
					</div>
				</div>
				<br>
				<div class="row buttons">
					<div class="button" id="town_center" zone_id = "2">Центр</div>
					<div class="button" id="town" zone_id = "1">Город</div>
					<div class="button" id="periferia" zone_id = "4">Периферия</div>
					<div class="button" id="km_price" zone_id = "3">Цена за км</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<!--		<p>Some text in the Modal..</p>-->
	</div>
</div>
<div class="modal_ask">
	<div id="review">
		Вы уверены, что хотите заказать эту поездку?
	</div>
	<br>
	<!--	<div class="definition">-->
	<!--		Вы заказали Renault Logan <br> Такси Сатурн поездка за <span class="dialog_price">1000 руб./чел.</span>-->
	<!--	</div>-->
	<!--	<div class="yep">-->
	<!--		Да, сделать заказ-->
	<!--	</div>-->
	<div class="nope">
		Закрыть
	</div>
	<div class="mini-close"></div>
</div>