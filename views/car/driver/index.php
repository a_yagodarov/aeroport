<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 24.06.2016
 * Time: 9:01
 */
use yii\helpers\Html;

$this->title = 'Водители '.\app\models\Car::getCarNameById($car_id);
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

	<?= $this->render('../_menu', [
		'car_id' => $car_id
	])?>

	<p>
		<?= Html::a('Добавить водителя', ['/driver-car/create', 'car_id' => $car_id], ['class' => 'btn btn-success']) ?>
	</p>


	<?= \kartik\grid\GridView::widget([
		'dataProvider' => $dataProvider,
//		'searchModel' => $searchModel,
		'columns' => [
			'driver.surname',
			'driver.name',
			'driver.middle_name',
			'driver.comment',
//			'driver.rating',
//			'license',
			[
				'class' => 'yii\grid\ActionColumn',
				'urlCreator'=>function($action, $model, $key, $index){
					return ['driver-car/'.$action,'id'=>$model->id];
				},
				'template' => '{update}, {delete}'
			],
		]
	])
	?>
</div>
