<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Car */
/* @var $form yii\widgets\ActiveForm */
//$jsFirst = <<< JS
//function readURL(input) {
//  if (input.files && input.files[0]) {
//  	img = $(input).parent().parent().next().children('img');
//    var reader = new FileReader();
//    reader.onload = function (e) {
//    	txt = $(input).attr('id');
//    	var numb = txt.match(/\d/g);
//		numb = numb.join("");
//		console.log(numb);
//		id = "#carinside-"+numb+"-id";
//		console.log(id);
//		$(id).remove();
//      	$(img).attr('src', e.target.result);
//    };
//    reader.readAsDataURL(input.files[0]);
//  }
//}
//JS;
$js = <<< JS
function triggerTariff(value){
if (value == 0){
	$('#dynamic-form > div.panel.panel-default > div.panel-body input').prop('disabled', false);
	$('#car-group_tariff_id').prop('disabled', true);
}
else if (value == 1){
	$('#car-group_tariff_id').prop('disabled', false);
	$('#dynamic-form > div.panel.panel-default > div.panel-body input').prop('disabled', true);
}
}
function disableByUseGroupTariff(){
	$('input[name="Car[use_group_tariff]"]').change(function() {
		console.log('value'+$("input[type='radio'][name='Car[use_group_tariff]']:checked").val());
        triggerTariff($(this).val());
    });
    triggerTariff($("input[type='radio'][name='Car[use_group_tariff]']:checked").val());
}
disableByUseGroupTariff();
JS;

$this->registerJs($js, yii\web\View::POS_END);
?>

<div class="car-form">

    <?php $form = ActiveForm::begin([
	    'options' => [
		    'enctype' => 'multipart/form-data',
		    'id' => 'dynamic-form'
	    ]
    ]); ?>

	<?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'brand_id')->dropDownList(\app\models\Brand::getBrandsForDropDown(), [
		'prompt' => '-- Выберите --'
	]); ?>

	<?= $form->field($model, 'client_type_id')->dropDownList(\app\models\TypeClient::getTypeClientsForDropDownList(), [
		'prompt' => '-- Выберите --'
	])?>

	<?= $form->field($model, 'image')->fileInput() ?>

	<?
		if ($model->image)
			echo Html::img(Yii::getAlias('/web/files/images/').$model->image, [
				'width' => '240px',
				'height' => '180px'
			]);
	?>

    <?= $form->field($model, 'company')->dropDownList(\app\models\CarBrand::getCarBrandForDropDownList(), [
	    'prompt' => '-- Выберите --'
    ]) ?>

    <?= $form->field($model, 'model')->textInput() ?>

    <?= $form->field($model, 'class')->dropDownList(\app\models\CarClass::getClassesForDropdownlist(), [
	    'prompt' => '-- Выберите --'
    ]) ?>

    <?= $form->field($model, 'year')->textInput() ?>

    <?= $form->field($model, 'color')->textInput() ?>

    <?= $form->field($model, 'number_of_passengers')->textInput() ?>

	<?= $form->field($model, 'baggage_count')->textInput() ?>

	<?= $form->field($model, 'report_documents')->radioList([
		'0' => 'Нет',
		'1' => 'Да'
	]) ?>

	<?= $form->field($model, 'bank_card_payment')->radioList([
		'0' => 'Нет',
		'1' => 'Да'
	]) ?>

	<?= $form->field($model, 'external_baggage')->radioList([
		'0' => 'Нет',
		'1' => 'Да'
	]) ?>

	<?= $form->field($model, 'license')->radioList([
		'0' => 'Нет',
		'1' => 'Да'
	]) ?>

	<?= $form->field($model, 'license_requisites')->textarea(); ?>

    <?= $form->field($model, 'baby_chair')->radioList([
	    '0' => 'Нет',
	    '1' => 'Да',
    ]) ?>

    <?= $form->field($model, 'conditioner')->radioList([
	    '0' => 'Нет',
	    '1' => 'Да',
    ]) ?>

<!--	--><?//= $form->field($model, 'permission_to_drive')->radioList([
//		'0' => 'Нет',
//		'1' => 'Да',
//	]) ?>

	<?= $form->field($model, 'use_group_tariff')->radioList(
		[
			'0' => 'Нет',
			'1' => 'Да',
		],
		[
			'id' => 'radio'
		]) ?>

	<?= $form->field($model, 'group_tariff_id')->dropDownList(\app\models\GroupTariff::getDropDownForGroupTariff(), [
		'prompt' => '--Выберите--'
	]);
	?>

<!--	<div class="panel panel-default">-->
<!--		<div class="panel-heading">-->
<!--			Тарифы-->
<!--		</div>-->
<!--		<div class="panel-body">-->
<!--			--><?//= $form->field($model->tariffs, 'town_center')->textInput() ?>
<!---->
<!--			--><?//= $form->field($model->tariffs, 'town')->textInput() ?>
<!---->
<!--			--><?//= $form->field($model->tariffs, 'km_price')->textInput() ?>
<!--		</div>-->
<!--	</div>-->
<!---->
<!--	<sup>* Данные полей серого цвета не сохраняются </sup>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
