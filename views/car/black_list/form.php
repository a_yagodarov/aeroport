<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CarBlackList */
/* @var $form ActiveForm */
?>
<div class="black_list-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'image') ?>
        <?= $form->field($model, 'video') ?>
        <?= $form->field($model, 'comment')->textarea() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- black_list-form -->
