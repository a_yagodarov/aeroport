<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 24.06.2016
 * Time: 21:04
 */

$this->title = 'Фото изутри авто';

?>
<?= $this->render('../_menu', [
	'car_id' => $car_id
])?>
<div>

	<p>
		<?= \yii\helpers\Html::a('Добавить фото', ['/car-inside/create', 'car_id' => $car_id], ['class' => 'btn btn-success']) ?>
	</p>

	<?= \kartik\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			[
				'attribute' => 'image',
				'format' => 'raw',
				'value' => function($model)
				{
					return $model->getImage();
				}
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'urlCreator'=>function($action, $model, $key, $index){
					return ['car-inside/'.$action,'id'=>$model->id,'car_id' => $model->car_id];
				},
				'template' => '{update}, {delete}'
			],
		]
	])?>
</div>
