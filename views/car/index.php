<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CarSearch */
/* @var $model app\models\Car */
/* @var $dataProvider yii\data\ActiveDataProvider */

$urlGetForm = \yii\helpers\Url::to('/car/get-form');

$this->title = Yii::t('app', 'Cars');
$this->params['breadcrumbs'][] = $this->title;
$js = <<< JS
$('.black_list').click(function(){
	$.ajax({
		url : '$urlGetForm',
		type : 'post',
		success : function (data){
			$('#black_list_modal_form .modal-body').empty();
			$('#black_list_modal_form .modal-body').append(data);
			$('#black_list_modal_form').modal();
		}
	});
});
JS;

$this->registerJs($js);
?>
<div class="car-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Car'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
	        [
		        'attribute' => 'company',
		        'label' => 'Автомобиль',
		        'value' => function($model){
			        return
				        \app\models\CarBrand::getCarBrandById($model->company).' '
				        .$model->model.' '.$model->number.' '.$model->color;
		        }
	        ],
//            'model',
//	        [
//		        'attribute' => 'class',
//		        'value' => function($model){
//			        return \app\models\Car::getClassById($model->class);
//		        }
//	        ],
//	        'number',
//            'year',
//	        [
//		        'attribute' => 'color',
//	        ],
//             'number_of_passengers',
	        [
		        'label' => 'Профиль',
		        'format' => 'raw',
		        'value' => function($model)
		        {
			        return Html::a(
				        Html::button('Профиль', ['class' => 'btn btn-sm btn-primary']),
				        \yii\helpers\Url::toRoute(['car/update', 'car_id' => $model->id])
			        );
		        }
	        ],
            [
	            'label' => 'Фото изнутри авто',
	            'format' => 'raw',
	            'value' => function($model)
	            {
		            return Html::a(
			            Html::button('Просмотреть фото', ['class' => 'btn btn-sm btn-default']),
			            \yii\helpers\Url::toRoute(['car/car-inside', 'car_id' => $model->id])
		            );
	            }
            ],
	        [
		        'label' => 'Водители',
		        'format' => 'raw',
		        'value' => function($model)
		        {
			        return Html::a(
				        Html::button('Просмотр водителей', ['class' => 'btn btn-sm btn-default']),
				        \yii\helpers\Url::toRoute(['car/driver', 'car_id' => $model->id])
			        );
		        }
	        ],
			[
				'attribute' => 'is_black_list',
				'label' => 'Черный список',
				'format' => 'raw',
				'value' => function($model)
				{
					$return = !$model->is_black_list ?
						Html::a(Html::button('Добавить в черный список', ['class' => 'btn btn-sm btn-danger']), \yii\helpers\Url::toRoute(['car-black-list/create', 'car_id' => $model->id])) :
						Html::a(Html::button('Удалить из черного списка', ['class' => 'btn btn-sm btn-success']), \yii\helpers\Url::toRoute(['car-black-list/delete', 'car_id' => $model->id]), []);
					return $return;
				}
			],
	        [
		        'attribute' => 'permission_to_drive',
		        'label' => 'Статус',
		        'format' => 'raw',
		        'value' => function($model)
		        {
					return $model->getButtonForActivate();
		        }
	        ],
	        [
		        'class' => 'yii\grid\ActionColumn',
		        'template' => '{delete}'
	        ],
        ],
    ]); ?>
</div>
