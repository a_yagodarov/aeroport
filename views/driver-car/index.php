<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DriverCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Driver Cars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-car-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Driver Car', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'driver_id',
            'car_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
