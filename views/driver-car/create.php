<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DriverCar */

$this->title = 'Добавить водителя';
$this->params['breadcrumbs'][] = [
	'label' => 'Водители', 'url' => \yii\helpers\Url::toRoute(['car/driver', 'car_id' => $car_id])
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-car-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
