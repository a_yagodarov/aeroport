<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DriverCar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-car-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'driver_id')->dropDownList(\app\models\Driver::getDriversForDropdownList(), [
	    'prompt' => 'Выберите водителя'
    ]) ?>

    <?= $form->field($model, 'mifar_card_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
