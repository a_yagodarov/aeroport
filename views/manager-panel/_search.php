<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PanelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'car_id') ?>

    <?= $form->field($model, 'driver_id') ?>

    <?= $form->field($model, 'use_new_tariffs') ?>

    <?= $form->field($model, 'town') ?>

    <?php // echo $form->field($model, 'town_center') ?>

    <?php // echo $form->field($model, 'km_price') ?>

    <?php // echo $form->field($model, 'station_time') ?>

    <?php // echo $form->field($model, 'date_create') ?>

    <?php // echo $form->field($model, 'date_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
