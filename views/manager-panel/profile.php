<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 12.07.2016
 * Time: 11:21
 * @var $model app\models\Panel
 */

$arr = [
	'0' => 'Нет',
	null => 'Нет',
	'1' => 'Да'
];
$this->title = 'Профиль';

if ($photos = $model->car->loadCarInsideImages())
{
	$i = 1;
	foreach($photos as $key => $value)
	{
		$attributes[] = [
			'attribute' => '',
			'label' => 'Фото изнутри '.$i,
			'format' => 'raw',
			'value' => \app\models\CarInside::getImageByName($model->car_id, $value)
		];
		$i++;
	}
}

$url = \yii\helpers\Url::to('/manager-panel/print');
$urlAjax = \yii\helpers\Url::toRoute(['/manager-panel/order', 'id' => $model->id]).'&zone=';
$urlDelete = \yii\helpers\Url::toRoute(['/manager-panel/delete', 'id' => $model->id]);

/* getting car info */
$carName = $model->car->getCarName(false);
$carBrand = $model->car->brand->name;
$reviewsText = $model->driver->reviews_text;
$js = <<< JS
carName = '$carName';
carBrand = '$carBrand';
reviewsText = '$reviewsText';
if (reviewsText == '')
	reviewsText = 'Ничего не найдено';
$('.question').html(reviewsText);
$('.button').click(function(){
	carTariff = $.trim($(this).html());
})
$('.book').click(function(){
	$('.modal_ask').fadeIn(1);
});
JS;

$this->registerJs($js);
?>

<div class="card_modal">

	<div class="car_mark_card">
		<?
		echo $model->car->getCarName();
		?>
	</div>
	<div style="float: left;float: left;width: 500px;">
		<div class="driver_info">Информация о водителе</div>
		<img class="driver_avatar" src="<?= $model->driver->getImagePath() ?>" alt="driver" width="200" height="auto">
		<div class="driver_name"><?= $model->driver->surname ?><br>
		<?= $model->driver->name ?><br>
		<?= $model->driver->middle_name ?></div>
		<div class="experience">Стаж за рулем - <?= $model->driver->experience ?></div>
		<table class="table_book">
			<tr>
				<td><div class="book">Книга отзывов</div></td>
				<td><div class="">
						<?
							echo str_repeat(' <span style="font-size: 25px;color: yellow;">&nbsp &#9733</span> ', $model->driver->rating);
							echo str_repeat(' <span style="font-size: 25px;">&nbsp &#9733 </span> ', 5-$model->driver->rating);
						?></div></td>
			</tr>
			<tr>
				<td>
					<div class="car_screens">
						<img class="car1" src="<?= $model->car->getImagePath() ?>" alt="">
					</div>
				</td>
				<td>
					<div class="car_screens">
						<? if ($photos){?><img class="car2" src="<?= \app\models\CarInside::getImagePathByName($model->car_id, array_shift($photos));?>" alt=""><?} ?>
					</div>
				</td>
			</tr>
		</table>



	</div>
	<div class="2_col">
		<div class="car_information">Информация об автомобиле</div>
		<table class="table_car">
			<tr>
				<td>Класс автомобиля&nbsp</td>
				<td><?= \app\models\Car::getClassById($model->car->class)?> </td>
			</tr>
			<tr>
				<td>Государственный номер&nbsp</td>
				<td><?= $model->car->number ?></td>
			</tr>
			<tr>
				<td>Год выпуска т.с&nbsp</td>
				<td><?= $model->car->year ?></td>
			</tr>
			<tr>
				<td>Количество мест&nbsp</td>
				<td><?= $model->car->number_of_passengers ?></td>
			</tr>
			<tr>
				<td>Наличие доп.багажника на крыше&nbsp</td>
				<td><?
					echo $arr[$model->car->external_baggage];
					?></td>
			</tr>
			<tr>
				<td>Наличие детского кресла&nbsp</td>
				<td><?= $arr[$model->car->baby_chair] ?></td>
			</tr>
			<tr>
				<td>Оплата банковскими картами&nbsp</td>
				<td><?= $arr[$model->car->bank_card_payment] ?></td>
			</tr>
			<tr>
				<td>Отчетные документы&nbsp</td>
				<td><?= $arr[$model->car->report_documents] ?></td>
			</tr>
		</table>
		<? if ($model->town_center){?>
			<div class="pr_2 button" zone="2" onclick="
				if (confirm('Вы уверены что хотите сделать заказ?')) {
				$.ajax({
					url: '<?= $urlAjax?>'+2,
				success : function(){
					console.log(123);
					window.location.replace('<?= $urlDelete?>')
				}
				});
				id=<?= $model->id ?>;
				zone=2;
				url='<?=$url?>'+'?id='+id+'&zone='+zone;
				var win = window.open(url, '_blank');
				win.focus();
				setTimeout(function (){
				window.location.replace('<?= $urlDelete?>')
				// Something you want delayed.
				}, 1000);}
				">Центр - <?
				if (intval($model->town_center))
					echo $model->town_center.' руб';
				else echo $model->town_center
				?>
			</div>
		<? if ($model->town){?>
		<div class="pr_1 button" zone="1" onclick="
			if (confirm('Вы уверены что хотите сделать заказ?')) {
			$.ajax({
			url: '<?= $urlAjax?>'+1,
			success : function(){
			console.log(123);
			window.location.replace('<?= $urlDelete?>')
			}
			});
			id=<?= $model->id ?>;
			zone=1;
			url='<?=$url?>'+'?id='+id+'&zone='+zone;
			var win = window.open(url, '_blank');
			win.focus();
			setTimeout(function (){
			window.location.replace('<?= $urlDelete?>')
			// Something you want delayed.
			}, 1000);}

			">Город - <?
			if (intval($model->town))
				echo $model->town.' руб';
			else echo $model->town
			?>
		</div>
		<?} ?>

		<?} ?>
		<? if ($model->periferia){?>
		<div class="pr_2 button" zone="4" onclick="
			if (confirm('Вы уверены что хотите сделать заказ?')) {
			$.ajax({
			url: '<?= $urlAjax?>'+4,
			success : function(){
			console.log(123);
			window.location.replace('<?= $urlDelete?>')
			}
			});
			id=<?= $model->id ?>;
			zone=4;
			url='<?=$url?>'+'?id='+id+'&zone='+zone;
			var win = window.open(url, '_blank');
			win.focus();
			setTimeout(function (){
			window.location.replace('<?= $urlDelete?>')
			// Something you want delayed.
			}, 1000);}
			">Периферия -
			<?
			if (intval($model->periferia))
				echo $model->periferia.' руб';
			else echo $model->periferia
			?>
		</div>
		<?} ?>
		<? if ($model->km_price){?>
		<div class="pr_3 button" zone="3" onclick="
				if (confirm('Вы уверены что хотите сделать заказ?')) {
		$.ajax({
			url: '<?= $urlAjax?>'+3
			});
			id=<?= $model->id ?>;
			zone=3;

			url='<?=$url?>'+'?id='+id+'&zone='+zone;
			var win = window.open(url, '_blank');
			win.focus();
			console.log(123);
			setTimeout(function (){
			window.location.replace('<?= $urlDelete?>')
			// Something you want delayed.
			}, 1000);}
			">
			Межгород - <?
			if (intval($model->km_price))
				echo $model->km_price.' руб/км';
			else echo $model->km_price
			?>
		</div>
		<?} ?>
	</div>
	<div class="close"></div>
</div>


