<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 08.07.2016
 * Time: 15:30
 */

$css = <<< CSS
.border-black {
	border:1px solid black;
}
body, html, table{
	height: 99%;
	width: 100%;
}
table{
	height: 98%;
	width: 100%;
}
CSS;
//$js = <<< JS
//print()
//JS;


$this->registerCss($css);
//$this->registerJs($js, \yii\web\View::POS_END);
/* @var $model app\models\Panel */
?>
<div id="ticket">
	<table style="text-align: center;" class="">
		<tr>
			<td colspan="2" style="font-size:25pt" class=" border-black">
				Талон № <strong><?= $model->id ?></strong><br>
				на посадку в такси
			</td>
		</tr>
		<tr >
			<td colspan="2" class=" border-black">
				<table style="text-align: center;" width="100%">
	<!--			<strong>-->
					<tr style="font-family: Verdana; font-size: 20pt">
						<td>Номер машины:</td>
					</tr>
					<tr style="font-family: Verdana; font-size: 35pt">
						<td><?= $model->car->number ?></td>
					</tr>
					<tr style="font-family: Arial; font-size: 25pt">
						<td>
							<?= $model->car->getCarBrand() ?>
							<?= $model->car->model?>
						</td>
					</tr>
					<tr style="font-family: Arial; font-size: 25pt">
						<td><?= $model->car->color?></td>
					</tr>
	<!--			</strong>-->
				</table>
			</td>
		</tr>
		<tr>
			<td style="font-family: Verdana; font-size: 14pt" class=" border-black">
				Бренд:
			</td>
			<td style="font-family: Verdana; font-size: 14pt" class=" border-black">
				<?= $model->car->brand->name ?>
			</td>
		</tr>
		<tr>
			<td colspan="2" class=" border-black">
				<strong style="font-family: Verdana; font-size: 20pt">тел. водителя: </strong><br>
				<p style="font-family: Verdana; font-size: 25pt"><?= $model->driver->phone_number?></p>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="for-dashed border-black">
				<strong style="font-family: Verdana; font-size: 20pt">Цены:</strong><br>
				<? if ($model->town_center){ ?>
					<p style="
					<? if ($zone == 1) echo "text-decoration:underline;"?>
						font-family: Calibri; font-size: 20pt">Центр - <?= \app\components\PriceWidget::widget(['price' => $model->town_center]); ?><br>
					</p>
				<?}?>
				<? if ($model->town){ ?>    
					<p style="
					<? if ($zone == 2) echo "text-decoration:underline;"?>
						font-family: Calibri; font-size: 20pt">Город - <?= \app\components\PriceWidget::widget(['price' => $model->town]); ?><br>
					</p>
				<?}?>
				<? if ($model->periferia){ ?>
					<p style="
					<? if ($zone == 4) echo "text-decoration:underline;"?>
						font-family: Calibri; font-size: 20pt">Периферия - <?= \app\components\PriceWidget::widget(['price' => $model->periferia]); ?><br>
					</p>
				<?}?>
				<? if ($model->km_price){ ?>
					<p style="
					<? if ($zone == 3) echo "text-decoration:underline;"?>
						font-family: Calibri; font-size: 20pt">межгород - <?= \app\components\PriceWidget::widget(['price' => $model->km_price]); ?>
					</p>
				<?}?>
			</td>
		</tr>
		<tr>
			<td colspan="2" class=" border-black">
				<strong style="font-family: Calibri; font-size: 20pt">Для обратной связи:</strong><br>
				<p style="font-family: Verdana; font-size: 20pt">89871458288</p>
				<p style="font-family: Verdana; font-size: 20pt">airavalon2007@yandex.ru</p>
			</td>
		</tr>
		<tr>
			<td style="font-family: Verdana; font-size: 20pt" width="50%" class=" border-black">
				Дата, <br>
				время
			</td>
			<td style="font-family: Verdana; font-size: 20pt" width="50%" class=" border-black">
				<?= date('d/m/Y')?><br>
				<?= date('G:i')?>
			</td>
		</tr>
	</table>
</div>