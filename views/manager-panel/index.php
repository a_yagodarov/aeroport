<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PanelSearch */
/* @var $model app\models\Car */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Стоянка';
$this->params['breadcrumbs'][] = $this->title;

$urlProfile = \yii\helpers\Url::toRoute('manager-panel/profile');
//$car = $model->getCar()->one();
$css = <<< CSS
#layer1{
    /*background: #fff000;*/
    width: 200px;
    height: 100px;
    position: fixed;
    top:0;
    right:0;
    visibility: visible;
}
.btn{
	font-size: 45;
	color:#00243a;
}
.btn:hover{
	color:yellow;
}
CSS;

$js = <<< JS
var carName;
var carBrand;
$('.cars .car_info td').mouseover(function(){
	$(this).parent().addClass('selected');
}).mouseout(function(e){
	$(this).parent().removeClass('selected');
});
	$(window).on("load",function(){
		$("#content-9").mCustomScrollbar({
			scrollButtons:{enable:true,scrollType:"stepped"},
			keyboard:{scrollType:"stepped"},
			mouseWheel:{scrollAmount:70},
			theme:"rounded-dark",
			autoExpandScrollbar:true,
			snapAmount:70,
			snapOffset:6
		});
	});
function hoverButtons(){
	$('.btn').mouseover(function(){
	$(this).attr('visibility', 'visible');
}).mouseout(function(e){
	$(this).attr('visibility', 'hidden');
});
}
$(".cars .car_info").click(function () {
    $('.main_container').addClass('blur');
    //console.log(this+123);
    getCarInfo($(this).attr('id'));
});
function addCloseClickFadeOut()
{
	$(".close").click(function () {
	    $('.main_container').removeClass('blur');
	    $('.card_modal').fadeOut(1);
	});
	$(".mini-close").click(function () {
	    $('.modal_ask').fadeOut(1);
	});

}
$(".border_3").click(function () {
    //$('.car_mark').addClass('blur');
    //$('.tax_name').addClass('blur');
    //$('.blur_title').addClass('blur');
    $('.modal_ask').fadeIn(1);
});
$(".nope").click(function () {
    //$('.car_mark').removeClass('blur');
    //$('.tax_name').removeClass('blur');
    //$('.blur_title').removeClass('blur');
    $('.modal_ask').fadeOut(1);
});

function getCarInfo(id){
	$.ajax({
		url : '$urlProfile',
		data : {
			id : id
		}
	}).done(function(data){
		result = JSON.parse(data);
		console.log(result);
		if (result['html'])
		{
			$('.card_modal').replaceWith(result['html']);
			//$('.card_modal').append();
			$('.card_modal').fadeIn(5);
			addCloseClickFadeOut();
		}
	});
}
hoverButtons();
JS;

$this->registerCssFile('/web/css/reset.min.css');
$this->registerCssFile('/web/css/main.css');
//$this->registerCssFile('/web/css/reset.css');
$this->registerCssFile('/web/css/jquery.mCustomScrollbar.css');
//$this->registerJs($js, \yii\web\View::POS_LOAD);
$this->registerJsFile("http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js");
$this->registerJsFile("/web/js/main.js");
$this->registerJsFile("/web/js/jquery.mCustomScrollbar.concat.min.js");
$this->registerJs($js, \yii\web\View::POS_END);
$this->registerCss($css);
$models = $dataProvider->getModels();
$x = $models[0]->car;
usort($models, function($a, $b){
	if ($a->driver->rating == $b->driver->rating) {
		if ($a->town == $b->town) {
			return 0;
		}
		return ($a->town < $b->town) ? -1 : 1;
	}
	return ($a->driver->rating > $b->driver->rating) ? -1 : 1;
});
$this->title = 'Стоянка';
?>
<div id="layer1">
	<h1><?= Html::a('Въезд авто', ['create'], ['class' => 'btn']) ?></h1>
	<h1><?= Html::a('Выезд авто', ['delete2'], ['class' => 'btn']) ?></h1>
</div>
<div class="main_container blure">
	<p><span class="blur_title">возле терминала стоит машин такси: <?= count($models) ?></span></p>

	<!-- content -->
	<div id="demo" class="showcase">
		<section id="examples">
			<div id="content-9" class="content vertical-images">
				<table class="cars">
					<tr class="line-height">
						<td class=""></td>
						<td class=""><span style="color: transparent">-</span></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
					<?
					foreach($models as $model)
					{
						echo '<tr class="car_info" id='.$model->id.'>
									<td class="star">';
						echo ($model->car->bank_card_payment) ? '<span class="span_point" style="color: green;">&bull;</span>' : null;
						echo ($model->car->baby_chair) ? '<span class="span_point" style="color: blue;">&bull;</span>' : null;
						echo ($model->car->external_baggage) ? '<span class="span_point" style="color: yellow;">&bull;</span>' : null;
						echo ($model->car->report_documents) ? '<span class="span_point" style="color: red;">&bull;</span>' : null;
						echo '</td>
								<td class="car_mark ">'.\app\models\Car::getCarCompanyAndModelById($model->car_id).'</td>
								<td class="tax_name  bordered">'.$model->car->brand->name.'</td>
								<td class="bordered cost "><span class="1000">'.$model->town.'
							</tr>';
					}
					?>


					<tr>
						<td class="star"></td>
						<td class="car_mark"><span style="color: transparent">-</span></td>
						<td class="tax_name bordered"></td>
						<td class="bordered cost"></td>
					</tr>
					<tr>
						<td class="star"></td>
						<td class="car_mark"><span style="color: transparent">-</span></td>
						<td class="tax_name bordered"></td>
						<td class="bordered cost"></td>
					</tr>
					<tr>
						<td class="star"></td>
						<td class="car_mark"><span style="color: transparent">-</span></td>
						<td class="tax_name bordered"></td>
						<td class="bordered cost"></td>
					</tr>
					<tr>
						<td class="star"></td>
						<td class="car_mark"><span style="color: transparent">-</span></td>
						<td class="tax_name bordered"></td>
						<td class="bordered cost"></td>
					</tr>
					<tr>
						<td class="star"></td>
						<td class="car_mark"><span style="color: transparent">-</span></td>
						<td class="tax_name bordered"></td>
						<td class="bordered cost"></td>
					</tr>
					<tr>
						<td class="star"></td>
						<td class="car_mark"><span style="color: transparent">-</span></td>
						<td class="tax_name bordered"></td>
						<td class="bordered cost"></td>
					</tr>
					<tr>
						<td class="star"></td>
						<td class="car_mark"><span style="color: transparent">-</span></td>
						<td class="tax_name bordered"></td>
						<td class="bordered cost"></td>
					</tr>


				</table>
			</div>

		</section>
	</div>

<!--	<div class="container"></div>-->
</div>

<div class="card_modal">

	<div class="car_mark_card">Peugeot 206</div>
	<div style="float: left;float: left;width: 474px;">
		<div class="driver_info">Информация о водителе</div>
		<img class="driver_avatar" src="https://freelance.ru/img/avatars/1376372.png" alt="driver">
		<div class="driver_name">Иванов Петр Степанович</div>
		<div class="experience">Стаж управления т.с - 12 лет</div>
		<table class="table_book">
			<tr>
				<td><div class="book">Книга отзывов</div></td>
				<td><div class="stars"></div></td>
			</tr>
			<tr>
				<td>
					<div class="car_screens">
						<img class="car1" src="img/car1.png" alt="">
					</div>
				</td>
				<td>
					<div class="car_screens">
						<img class="car2" src="img/car2.png" alt="">
					</div>
				</td>
			</tr>
		</table>



	</div>
	<div class="2_col">
		<div class="car_information">Информация об автомобиле</div>
		<table class="table_car">
			<tr>
				<td>Класс автомобиля</td>
				<td>С </td>
			</tr>
			<tr>
				<td>Государственный номер</td>
				<td>А 062 НХ 177 RUS</td>
			</tr>
			<tr>
				<td>Год выпуска т.с</td>
				<td>2012</td>
			</tr>
			<tr>
				<td>Количество мест</td>
				<td>5</td>
			</tr>
			<tr>
				<td>Наличие доп.багажника на крыше</td>
				<td>Есть</td>
			</tr>
			<tr>
				<td>Наличие детского кресла</td>
				<td>Есть</td>
			</tr>
			<tr>
				<td>Наличие лицензии</td>
				<td>Есть</td>
			</tr>
		</table>
		<div class="pr_1">Город - 450 руб.</div>
		<div class="pr_2">Центр - 300 руб.</div>
		<div class="pr_3">Межгород - 1 000 руб.</div>
	</div>
	<div class="close"></div>
</div>

<div class="modal_ask">
	<div class="question">
		Вы уверены, что хотите заказать эту поездку?
	</div>
	<br>
<!--	<div class="definition">-->
<!--		Вы заказали Renault Logan <br> Такси Сатурн поездка за <span class="dialog_price">1000 руб./чел.</span>-->
<!--	</div>-->
<!--	<div class="yep">-->
<!--		Да, сделать заказ-->
<!--	</div>-->
	<div class="nope">
		Закрыть
	</div>
	<div class="mini-close"></div>
</div>