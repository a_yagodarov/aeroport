<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 08.07.2016
 * Time: 14:28
 */

/* @var $model app\models\Panel */
/* @var $order app\models\OrderForm */
/* @var $zone */
use yii\widgets\ActiveForm;


$id = $_GET['id'];
$this->title = 'Заказать';
$this->params['breadcrumbs'][] = ['label' => 'Стоянка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<< JS
$('#print').on('click', function()
{
	var printContents = $('#ticket').html();
    var originalContents = $('body').html();
    document.body.innerHTML = printContents;
    $('body').css('overflow', 'hidden');
    window.print();
    $('body').css('overflow', 'scroll');
  	document.body.innerHTML = originalContents;
});
JS;

$this->registerJs($js, \yii\web\View::POS_END);
?>

<h3>Заказать</h3>
<div class="order-car">
<div style="width:60%">
<?= $this->render('print', [
	'model' => $model,
	'zone' => $zone
]) ?>
</div>
	<?
	\yii\widgets\Pjax::begin([
		'id' => 'order-form',
//		'linkSelector' => 'aeroport/panel/index'
	]);
	$form = ActiveForm::begin([
		'options' => [
			'data-pjax' => true
		],
	])
	?>
	<?
	if ($message)
		echo \yii\bootstrap\Html::tag('div', $message, [
//			'class' => 'alert alert-success',
//			'style' => 'height:50px'
		])
	?>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<?= \yii\helpers\Html::button('Распечатать талон', [
				'id' => 'print',
				'class' => 'btn btn-primary',
				'data-pjax' => 0,
	//			'onclick' => 'alert(123)'
			])?>
			<?= \yii\helpers\Html::submitButton('Заказать', [
				'id' => 'send-sms',
				'type' => 'submit',
				'onClick'=>\app\models\OrderMessage::findOne(['panel_id' => $id]) ? '' : 'return confirm("Отправить смс и убрать из стоянки?");',
//				'data-pjax' => 0,
				'class' => \app\models\OrderMessage::findOne(['panel_id' => $id]) ? 'btn btn-success disabled' : 'btn btn-success',
//				'method' => 'post'
			])?>
			<?= \yii\bootstrap\Html::a(\yii\bootstrap\Html::button('Назад', [
					'class' => 'btn btn-warning',
				]),
				[
					'index'
				],
				[
					'data-pjax' => 0
				])?>
		</div>
	</div>
	<? ActiveForm::end()?>
	<? \yii\widgets\Pjax::end()?>
</div>
