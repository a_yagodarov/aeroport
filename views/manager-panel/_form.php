<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Panel */
/* @var $form yii\widgets\ActiveForm */
$urlGetDrivers = \yii\helpers\Url::to('/driver/get-drop-down-drivers');
$urlGetTariff = \yii\helpers\Url::to('/panel/get-cars-tariff');
$script = <<< JS
function replaceCarsDropDown()
{
	$.ajax({
		url : '$urlGetDrivers',
		type : 'post',
		data : {
			id : $('#panel_car_value_select2').val(),
			//order_id: $model->id
		},
		success : function(data){
			$('#panel-driver_id').empty();
			$('#panel-driver_id').append($(data).children());
		}
	})
}

function getTariffPrice()
{
	$.ajax({
		url : '$urlGetTariff',
		type : 'post',
		data : {
			id : $('#panel_car_value_select2').val(),
		},
		success : function(data){
			data = JSON.parse(data);
			console.log(data);
			if (data['name'])
			{
				$('#panel-town').val(data['town']);
				$('#panel-town_center').val(data['town_center']);
				$('#panel-periferia').val(data['periferia']);
				$('#panel-km_price').val(data['km_price']);
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('disabled', true);
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('readonly', true);
				$('#w0 > div.panel.panel-default input').attr('disabled', true);
				return true;
			}
			else if (data != null)
			{
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('readonly', false);
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('disabled', false);
				$('#w0 > div.panel.panel-default input').attr('disabled', false);

				$('#panel-town').val(data['town']);
				$('#panel-town_center').val(data['town_center']);
				$('#panel-periferia').val(data['periferia']);
				$('#panel-km_price').val(data['km_price']);
			}
			else {
				alert('Тарифы не найдены! ... ');
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('readonly', false);
				$('#w0 > div.form-group.field-panel-use_new_tariffs input').attr('disabled', false);
				$('#w0 > div.panel.panel-default input').attr('disabled', false);

				$('#panel-town').val('');
				$('#panel-town_center').val('');
				$('#panel-periferia').val('');
				$('#panel-km_price').val('');
			}
		}
	});
}
$('#panel_car_value_select2').on('change', function(){
	console.log(this+$(this).val());
	$('#panel_car_value_select2').val($(this).val());
	replaceCarsDropDown();
	getTariffPrice()
});
replaceCarsDropDown();
//disableIfGroupTariff();

if ('$model->isNewRecord' == 1)
	getTariffPrice();
	console.log('$model->isNewRecord');
//$('.main-form').removeClass('hidden');
$('#select2-panel_car_value_select2-container').focus();
JS;


$this->registerJs($script, \yii\web\View::POS_END);
?>

<div class="panel-form main-form">

	<?php $form = ActiveForm::begin([
//	    'enableAjaxValidation' => true
	]); ?>

	<?= $form->field($model, 'car_id')->widget(Select2::className(), [
		'data' => \app\models\Car::getCarsForDropDownList([
			'panel' => true,
		]),
		'options' => [
			'id' => 'panel_car_value_select2',
			'placeholder' => 'Выберите авто ...',
//			'enableAjaxValidation' => true
		],
		'pluginOptions' => [
			'allowClear' => true
		]
	]) ?>

	<?= $form->field($model, 'driver_id')->dropDownList([],
		[
			'prompt' => '-- Выберите --'
		])	?>

	<!--	--><?//= $form->field($model, 'car_id')->dropDownList([], ['prompt' => 'Выберите авто'])	?>

	<?= $form->field($model, 'station_time')->textInput() ?>

	<?= $form->field($model, 'use_new_tariffs')->checkbox() ?>

	<div class="panel panel-default">
		<div class="panel-heading">
			Тарифы
		</div>
		<div class="panel-body">
			<?= $form->field($model, 'town_center')->textInput() ?>

			<?= $form->field($model, 'town')->textInput() ?>

			<?= $form->field($model, 'periferia')->textInput() ?>

			<?= $form->field($model, 'km_price')->textInput() ?>
		</div>
	</div>

	<!--	<sup>* Данные полей серого цвета не  </sup>-->

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Применить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
