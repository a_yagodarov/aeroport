<?php

use yii\helpers\Html;
use kartik\select2\Select2;
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 04.07.2016
 * Time: 18:12
 */

$js = <<< JS
function addChangeListener()
{
	$('#panel_car_value_select2').on('change', function(){
		val = $('#panel_car_value_select2').val();

	})
}
JS;


?>
<?php $form = \yii\widgets\ActiveForm::begin([

]); ?>
<?= $form->field($model, 'car_id')->widget(Select2::className(), [
	'data' => \app\models\Panel::getCarsInPanel(),
	'options' => [
		'id' => 'panel_car_value_select2',
		'placeholder' => 'Выберите авто ...',
//			'enableAjaxValidation' => true
	],
	'pluginOptions' => [
		'allowClear' => true
	]
]) ?>
<div class="form-group">
	<?= Html::submitButton('Выезд', ['class' => 'btn btn-warning']) ?>
</div>
<? \yii\widgets\ActiveForm::end()?>
