<?php
use \yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 04.07.2016
 * Time: 18:11
 */

$this->title = 'Выезд авто';
$this->params['breadcrumbs'][] = ['label' => 'Стоянка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="delete-panel">

	<h3><?= Html::encode($this->title) ?></h3>

	<?= $this->render('_delete_form', [
		'model' => $model,
	]) ?>

</div>
