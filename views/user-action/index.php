<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Действия пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-action-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
	    'columns' => [
//		    ['class' => 'yii\grid\SerialColumn'],
	      [
		      'attribute' => 'id'
	      ],
		    [
			    'attribute' => 'action_id',
			    'value' => function($model){
				    return $model->getActionById($model->action_id);
			    },
			    'filter' => Html::dropDownList('UserActionSearch[action_id]', $searchModel->action_id, \app\models\UserAction::getActionsForDropDown(), [
				    'class' => 'form-control',
				    'prompt' => 'Выберите'
			    ])
		    ],
		    [
			    'attribute' => 'text'
		    ],
		    [
			    'attribute' => 'manager_id',
			    'value' => function($model) {
				    return $model->manager->username;
			    },
			    'filter' => Html::dropDownList('UserActionSearch[manager_id]', $searchModel->manager_id, \app\models\User::getUserListForDropDownList(), [
				    'class' => 'form-control',
				    'prompt' => 'Выберите'
			    ])
		    ],
		    [
			    'attribute' => 'date_create',
			    'label' => 'Дата',
			    'format' => 'datetime',
			    'filter' => \kartik\date\DatePicker::widget([
				    'model' => $searchModel,
				    'attribute' => 'date_create',
				    //'language' => 'ru',
				    //'dateFormat' => 'yyyy-MM-dd',
			    ])
		    ]

//            ['class' => 'yii\grid\ActionColumn'],
	    ],
    ]); ?>
</div>
