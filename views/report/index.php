<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//	        'date_create:datetime',
	        [
		        'attribute' => 'date_create',
		        'label' => 'Дата',
		        'format' => 'datetime',
		        'value' => 'date_create',
		        'filter' =>
			        'От '.DatePicker::widget([
	    'model' => $searchModel,
	    'attribute' => 'date_from',
	    //'language' => 'ru',
	    //'dateFormat' => 'yyyy-MM-dd',
    ]).
			        ' До '.DatePicker::widget([
				        'model' => $searchModel,
				        'attribute' => 'date_to',
				        //'language' => 'ru',
				        //'dateFormat' => 'yyyy-MM-dd',
			        ])
//			        DatePicker::widget([
//				        'name'=>'ReportSearch[date_from]',
//				        'value' => $searchModel->date_from ? $searchModel->date_from : '',
//				        'name2'=>'ReportSearch[date_to]',
//				        'value2' => $searchModel->date_to ? $searchModel->date_to : '',
//				        'type' => DatePicker::TYPE_RANGE,
//				        'separator' => 'до',
//				        'pluginOptions' => [
//					        'autoclose'=>false,
//					        'format' => 'dd-mm-yyyy'
//				        ]
//			        ])
	        ],
	        [
		        'attribute' => 'action_id',
		        'value' => function($model)
		        {
					return $model->action->name;
		        },
		        'filter' => \app\models\Action::getActionsForDropDown()
	        ],
            'description',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
