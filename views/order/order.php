<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 08.07.2016
 * Time: 14:28
 */

/* @var $model app\models\Panel */
/* @var $order app\models\OrderForm */
/* @var $zone */
use yii\widgets\ActiveForm;


$id = $_GET['id'];
$zone = $_GET['zone'];
$this->title = 'Заказ № '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$urlAjax = \yii\helpers\Url::toRoute(['/manager-panel/send-sms', 'id' => $model->id]).'&zone=';

$js = <<< JS
$('#print').on('click', function()
{
	var printContents = $('#ticket').html();
    var originalContents = $('body').html();
    document.body.innerHTML = printContents;
    $('body').css('overflow', 'hidden');
    window.print();
    $('body').css('overflow', 'scroll');
  	document.body.innerHTML = originalContents;
});

$('#send-sms').on('click', function()
{
	if (confirm('Вы уверены что хотите повторно отправить смс?')) {
		$.ajax({
		url: '$urlAjax'+$zone,
		success : function(){
		console.log(123);
		}
		});
	}
});
JS;

$this->registerJs($js, \yii\web\View::POS_END);
?>

<h3><?= $this->title?></h3>
<div class="order-car">
<div style="width:60%">
<?= $this->render('/manager-panel/print2', [
	'model' => $model,
	'zone' => $zone
]) ?>
</div>
	<?

	?>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<?= \yii\helpers\Html::button('Распечатать талон', [
				'id' => 'print',
				'class' => 'btn btn-primary',
//				'data-pjax' => 0,
	//			'onclick' => 'alert(123)'
			])?>
			<?= \yii\helpers\Html::submitButton('Отправить смс', [
				'id' => 'send-sms',
//				'onClick'=>'sendSms()',
				'class' => 'btn btn-success',
//				'data-pjax' => 0
//				'method' => 'post'
			])?>
			<?= \yii\bootstrap\Html::a(\yii\bootstrap\Html::button('Назад', [
					'class' => 'btn btn-warning',
				]),
				[
					'index'
				])?>
		</div>
	</div>
</div>
