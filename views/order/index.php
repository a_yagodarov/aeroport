<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PanelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;

$urlProfile = \yii\helpers\Url::toRoute('/order/profile');
//$car = $model->getCar()->one();
$js = <<< JS
$('.car-profile').click(function(){
	var id = $(this).attr('id');
	$.ajax({
		url : '$urlProfile',
		data : {
			id : id
		}
	}).done(function(data){
		result = JSON.parse(data);
		console.log(result);
		if (result['html'])
		{
			$('#profile-panel').modal();
			$('#profile-panel .modal-body').empty();
			$('#profile-panel .modal-body').append(result['html']);
		}
	});
})
JS;

$this->registerJs($js, \yii\web\View::POS_LOAD)
?>
<div class="panel-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--	<p>-->
<!--		--><?//= Html::a('Въезд авто', ['create'], ['class' => 'btn btn-success']) ?>
<!--	</p>-->
   <?= GridView::widget([
		'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
		'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'car_id',
			[
				'attribute' => '',
				'format' => 'raw',
				'label' => 'Фото автомобиля',
				'value' => function($model)
				{
					return Html::img(Yii::getAlias('/web/files/images/'.$model->getCar()->one()->image), [
						'width' => '100px',
						'height' => '80px'
					]);
				}
			],
			[
				'attribute' => '',
				'format' => 'raw',
				'label' => 'Марка, модель',
				'value' => function($model)
				{
					$car = $model->getCar()->one();
					return
						Html::tag(
							'button',
							'<b>'.$car->getCarBrand().' '.$car->model.'</b>', [
							'class' => 'btn btn-default car-profile',
							'id' => $model->id
						])
//						.Html::tag('div', $car->number).
//						Html::tag('div', 'Пассажиры: '.$car->number_of_passengers).
//						Html::tag('div', 'Багажных мест: '.$car->baggage_count).
//						Html::tag('div', 'Кондиционер: '.$car->getConditioner()).
//						Html::tag('div', 'Детское кресло: '.$car->getChair())
						;
				}
			],
			[
				'attribute' => '',
				'label' => 'Бренд',
				'value' => function($model)
				{
					$brand = $model->brand->name;
					return ($brand);
//					return ($model->brand);
				}
			],
			[
				'attribute' => 'town_center',
		        'format' => 'raw',
				'label' => 'Уфа(центр)',
				'value' => function($model)
				{
					if ($model->town_center)
					return Html::a(
						Html::button($model->town_center, ['class' => 'btn btn-primary']),
						\yii\helpers\Url::toRoute([
							'order',
							'id' => $model->id,
							'zone' => 1
						])
					);
				}
			],
			[
				'attribute' => 'town',
				'format' => 'raw',
				'label' => 'Уфа',
				'value' => function($model)
				{
					if ($model->town)
					return Html::a(
						Html::button($model->town, ['class' => 'btn btn-primary']),
						\yii\helpers\Url::toRoute([
							'order',
							'id' => $model->id,
							'zone' => 2
						])
					);
				}
			],
			[
				'attribute' => 'periferia',
				'format' => 'raw',
				'label' => 'Периферия',
				'value' => function($model)
				{
					if ($model->periferia)
						return Html::a(
							Html::button($model->periferia, ['class' => 'btn btn-primary']),
							\yii\helpers\Url::toRoute([
								'order',
								'id' => $model->id,
								'zone' => 4
							])
						);
				}
			],
			[
				'attribute' => 'km_price',
				'format' => 'raw',
				'label' => 'Межгород',
				'value' => function($model)
				{
					if ($model->km_price)
					return Html::a(
						Html::button($model->km_price, ['class' => 'btn btn-primary']),
						\yii\helpers\Url::toRoute([
							'order',
							'id' => $model->id,
							'zone' => 3
						])
					);
				}
			],
		],
	]); ?>
	<div id="profile-panel" class="modal fade" role="dialog" >
		<div class="modal-dialog" style="width:80%">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Профиль авто</h4>
				</div>
				<div class="modal-body">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				</div>
			</div>

		</div>
	</div>
</div>
