<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 12.07.2016
 * Time: 11:21
 * @var $model app\models\Panel
 */
use \app\models\Car;
$this->title = 'Профиль';
$arr = [
	'0' => 'Нет',
	null => 'Нет',
	'1' => 'Да'
];
$attributes = [[
	'attribute' => 'car_id',
	'label' => 'Марка, модель',
	'value' => Car::getCarNameById($model->car->company)
],
	[
		'attribute' => '',
		'label' => 'Класс',
		'value' => \app\models\Car::getClassById($model->car->class)
	],
	[
		'attribute' => '',
		'label' => 'Номер',
		'value' => $model->car->number
	],
//	[
//		'attribute' => '',
//		'label' => 'Фото',
//		'format' => 'raw',
//		'value' => $model->car->getImage()
//	],
	[
		'attribute' => '',
		'label' => 'Год выпуска',
		'value' => $model->car->year
	],
//			[
//				'attribute' => '',
//				'label' => 'Цвет',
//				'value' => $model->car->color
//			],
	[
		'attribute' => '',
		'label' => 'Кол-во пассажирских мест',
		'value' => $model->car->number_of_passengers
	],
	[
		'attribute' => '',
		'label' => 'Наличие доп.багажника на крыше',
		'value' => $arr[$model->car->external_baggage]
	],
	[
		'attribute' => '',
		'label' => 'Фото водителя',
		'format' => 'raw',
		'value' => $model->driver->getImage()
	],
	[
		'attribute' => '',
		'label' => 'ФИО водителя',
		'value' => $model->driver->getFullName()
	],
	[
		'attribute' => '',
		'label' => 'Стаж водителя(лет)',
		'value' => $model->driver->experience
	],
];
if ($photos = $model->car->loadCarInsideImages())
{
	$i = 1;
	foreach($photos as $key => $value)
	{
		$attributes[] = [
			'attribute' => '',
			'label' => 'Фото изнутри '.$i,
			'format' => 'raw',
			'value' => \app\models\CarInside::getImageByName($model->car_id, $value)
		];
		$i++;
	}
}
else
{
	$attributes[] = [
		'attribute' => '',
		'value' => ''
	];
}
$attributes2 = [[
	'attribute' => '',
	'label' => 'Фото чистоты машины',
	'value' => ''
],
	[
		'attribute' => '',
		'label' => 'Наличие детского кресла',
		'value' => $arr[$model->car->baby_chair]
	],
	[
		'attribute' => '',
		'label' => 'Наличие лицензии',
		'value' => $arr[$model->driver->license]
	],
	[
		'attribute' => '',
		'label' => 'Рейтинг водителя',
		'value' => $model->driver->rating
	],
	[
		'attribute' => '',
		'label' => 'Претензии и жалобы на водителя',
		'value' => $model->driver->reviews_text
	],
	[
		'attribute' => '',
		'value' => false,
		'attributes' => [
			'attribute' => '',
			'value' => 123
		]
	],
	'town',
	'town_center',
	'km_price'];
$attributes = array_merge($attributes, $attributes2);
?>

<div class="profile">
	<h3> Профиль авто <?= \app\models\Car::getCarNameById($model->car_id)?></h3>
	<?= \kartik\detail\DetailView::widget([
		'model' => $model,
		'attributes' => $attributes
	]) ?>
</div>
