<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GroupTariff */
/* @var $form yii\widgets\ActiveForm */

$js = <<< JS
var selector = "[name='GroupTariff[use_single_tariff]']";
function enabledisableFields()
{
	val = $(selector+":checked").val();
	if (val == 1)
	{
		$('#grouptariff-periferia').attr('disabled', true);
		$('#grouptariff-town_center').attr('disabled', true);
		$('#grouptariff-km_price').attr('disabled', true);
	}
	else
	{
		$('#grouptariff-periferia').attr('disabled', false);
		$('#grouptariff-town_center').attr('disabled', false);
		$('#grouptariff-km_price').attr('disabled', false);
	}
}
$("[name='GroupTariff[use_single_tariff]']").on('change', function(){
	enabledisableFields()
});
enabledisableFields();
JS;

$this->registerJs($js);
?>

<div class="group-tariff-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'town_center')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'town')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'use_single_tariff')->radioList([
		'0' => 'Нет',
		'1' => 'Да'
	]) ?>

    <?= $form->field($model, 'periferia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'km_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
