<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GroupTariff */

$this->title = 'Создать групповой тариф';
$this->params['breadcrumbs'][] = ['label' => 'Групповые тарифы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-tariff-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
