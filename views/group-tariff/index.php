<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupTariffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Групповые тарифы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-tariff-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить групповой тариф', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'town_center',
            'town',
	          'periferia',
            'km_price',
            // 'comment',

            [
	            'class' => 'yii\grid\ActionColumn',
	            'template' => '{update}, {delete}'
            ],
        ],
    ]); ?>
</div>
