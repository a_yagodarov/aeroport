<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GroupTariff */

$this->title = 'Обновить групповой тариф: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Групповые тарифы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="group-tariff-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
