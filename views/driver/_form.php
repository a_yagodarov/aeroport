<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Driver */
/* @var $form yii\widgets\ActiveForm */

$js = <<< JS
var selector = "[name='DriverTariff[use_single_tariff]']";
function enabledisableFields()
{
	val = $(selector+":checked").val();
	if (val == 1)
	{
		$('#drivertariff-periferia').attr('disabled', true);
		$('#drivertariff-town_center').attr('disabled', true);
		$('#drivertariff-km_price').attr('disabled', true);
	}
	else
	{
		$('#drivertariff-periferia').attr('disabled', false);
		$('#drivertariff-town_center').attr('disabled', false);
		$('#drivertariff-km_price').attr('disabled', false);
	}
}
$("[name='DriverTariff[use_single_tariff]']").on('change', function(){
	enabledisableFields()
});
enabledisableFields();
JS;

$this->registerJs($js);

?>

<div class="driver-form">

    <?php $form = ActiveForm::begin([
	    'options' => [
		    'enctype' => 'multipart/form-data',
		    'id' => 'driver-form'
	    ]
    ]); ?>

	<?= $form->field($model, 'photo_image')->fileInput() ?>

	<?
	if ($model->photo_image)
		echo $model->getImage();
	?>

	<?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'driver_card_serial_number')->textInput(['maxlength' => true])?>

	<?= $form->field($model, 'passport_number')->textInput(['maxlength' => true])?>

	<? if ($model->car) :?>

		<div class="panel panel-default">
			<div class="panel-heading">
				Привязка к авто
			</div>
			<div class="panel-body">

				<?= $form->field($model->car, 'car_id')->dropDownList(\app\models\Car::getCarsForDropDownList(), [
					'prompt' => '-- Выберите --'
				]) ?>

				<?= $form->field($model->car, 'mifar_card_id')->textInput() ?>

			</div>
		</div>

	<? endif ?>

	<div class="panel panel-default">
		<div class="panel-heading">
			Тарифы
		</div>
		<div class="panel-body">

			<?= $form->field($model->tariffs, 'town_center')->textInput() ?>

			<?= $form->field($model->tariffs, 'town')->textInput() ?>

			<?= $form->field($model->tariffs, 'use_single_tariff')->radioList([
				'0' => 'Нет',
				'1' => 'Да'
			]) ?>

			<?= $form->field($model->tariffs, 'periferia')->textInput() ?>

			<?= $form->field($model->tariffs, 'km_price')->textInput() ?>

		</div>
	</div>
    <?= $form->field($model, 'phone_number')->textInput() ?>

    <?= $form->field($model, 'experience')->textInput() ?>

<!--    --><?//= $form->field($model, 'rating')->textInput([
//	    'disabled' => true
//    ]) ?>

	<?= $form->field($model, 'mvd_review_data')->textarea() ?>

	<?= $form->field($model, 'reviews_text')->textarea() ?>

	<?= $form->field($model, 'rating')->textInput() ?>

	<?= $form->field($model, 'comment')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
