<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CarInside */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-inside-form">

    <?php $form = ActiveForm::begin([
		    'options' => [
			    'enctype' => 'multipart/form-data',
			    'id' => 'car_inside_form'
		    ]
    ]); ?>

<!--    --><?//= $form->field($model, 'car_id')->textInput() ?>

    <?= $form->field($model, 'image')->fileInput() ?>

	<?
		if ($model->image)
			echo $model->getImage();
	?>

    <div class="form-group">
	    <br>
        <?= Html::submitButton($model->isNewRecord ? 'Применить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
