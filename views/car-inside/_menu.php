<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 23.06.2016
 * Time: 15:42
 */

?>

<?
	echo 'Автомобиль '.\app\models\Car::getCarNameById($car_id);
?>

<?

echo \yii\bootstrap\Nav::widget([
	'options' => [
		'class' => 'nav-tabs',
		'style' => 'margin-bottom: 15px',
	],
	'items' => [
		[
			'label' => 'Профиль',
			'url' => ['car/update', 'car_id' => $car_id]
		],
		[
			'label' => 'Фото изнутри авто',
			'url' => ['car/car-inside', 'car_id' => $car_id]
		],
		[
			'label' => 'Водители',
			'url' => ['car/driver', 'car_id' => $car_id]
		]
	],
//	'options' => ['class' =>'nav-pills']
]);

?>
