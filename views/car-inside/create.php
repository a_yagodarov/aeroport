<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CarInside */

$this->title = 'Добавить фото';
$this->params['breadcrumbs'][] = ['label' => 'Фото авто '.\app\models\Car::getCarNameById($car_id), 'url' => ['car/car-inside', 'car_id' => $car_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-inside-create">

<!--	--><?//= $this->render('_menu', [
//		'car_id' => $model->id
//	])?>

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
