<?php

namespace app\controllers;

use Yii;
use app\models\CarInside;
use app\models\CarInsideSearch;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CarInsideController implements the CRUD actions for CarInside model.
 */
class CarInsideController extends Controller
{
    /**
     * @inheritdoc
     */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'ruleConfig' => [
					'class' => AccessRule::className(),
				],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
						'matchCallback' => function(){
							return in_array(Yii::$app->user->identity->role_id, [
								\app\models\User::ROLE_ADMIN,
//						        \app\models\User::DISPATCHER,
							]);
						}
					],
				],
			]
		];
	}

    /**
     * Lists all CarInside models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarInsideSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarInside model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarInside model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($car_id)
    {
        $model = new CarInside();
		$model->car_id = $car_id;
        if ($model->load(Yii::$app->request->post())) {
	        $model->car_id = $car_id;
	        $model->image = UploadedFile::getInstance($model, 'image');
	        $model->uploadImage();
	        if ($model->save())
                return $this->redirect(['/car/car-inside', 'car_id' => $model->car_id]);
	        else
		        return $this->render('create', [
			        'car_id' => $car_id,
			        'model' => $model
		        ]);
        } else {
            return $this->render('create', [
	            'car_id' => $car_id,
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing CarInside model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($car_id, $id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
	        $model->car_id = $car_id;
	        $model->image = UploadedFile::getInstance($model, 'image');
	        if ($model->uploadImage())
		        $model->deleteImage();
	        if ($model->save())
		        return $this->redirect(['/car/car-inside', 'car_id' => $model->car_id]);
	        else
		        return $this->render('create', [
			        'car_id' => $car_id,
			        'model' => $model
		        ]);
        } else {
            return $this->render('update', [
                'model' => $model,
	            'car_id' => $car_id
            ]);
        }
    }

    /**
     * Deletes an existing CarInside model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    $model = $this->findModel($id);
        $model->deleteImage();
		$model->delete();
        return $this->redirect(['/car/car-inside', 'car_id' => $model->car_id]);
    }

    /**
     * Finds the CarInside model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarInside the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarInside::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
