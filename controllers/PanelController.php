<?php

namespace app\controllers;

use app\models\Car;
use app\models\CarTariff;
use app\models\DriverCar;
use app\models\GroupTariff;
use app\models\MessageForm;
use app\models\Order;
use app\models\PanelDeleteForm;
use app\models\User;
use Yii;
use app\models\Panel;
use app\models\PanelSearch;
use yii\base\Response;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * PanelController implements the CRUD actions for Panel model.
 */
class PanelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
	        'access' => [
		        'class' => AccessControl::className(),
		        'ruleConfig' => [
			        'class' => AccessRule::className(),
		        ],
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['@'],
				        'matchCallback' => function(){
					        return in_array(Yii::$app->user->identity->role_id, [
						        \app\models\User::ROLE_ADMIN,
						        \app\models\User::ROLE_MANAGER,
						        \app\models\User::DISPATCHER
					        ]);
				        }
			        ],
			        [
				        'allow' => true,
				        'roles' => ['@'],
				        'actions' => ['get-cars-tariff'],
				        'matchCallback' => function(){
					        return in_array(Yii::$app->user->identity->role_id, [
						        \app\models\User::ROLE_MANAGER
					        ]);
				        }
			        ],
						[
								'allow' => true,
								'roles' => ['*', '?', '@'],
								'verbs' => ['post', 'get'],
								'actions' => [
										'get-info', 'add'
								]
						],
		        ],
	        ],
	        // 'verbs' => [
	        //     'class' => \yii\filters\VerbFilter::className(),
	        //     'actions' => [
	        //         'get-info' => ['get', 'post'],
	        //         'add' => ['get', 'post']
	        //     ],
	        // ]
        ];
    }

	public function beforeAction($action)
	{
		if ($action->id == 'get-info' || $action->id == 'add')
			return true;
	    return parent::beforeAction($action);
	}

	public function actionOrder($id, $zone)
	{
		$model = $this->findModel($id);
		$order = new MessageForm([
			'zone' => $zone,
		]);
		if (Yii::$app->request->isAjax)
		{
			$order->zone = $zone;
			$order->panel_id = $id;
			$order->place = Yii::$app->user->identity->terminal_id ? Yii::$app->user->identity->terminal_id : 1;
			if ($order->validate() && $order->sendMessage())
			{
				return $this->redirect([
					'delete',
					'id' => $id,
				]);
			}
			else
			{
				return var_dump($order);
				return $this->render('order', [
					'model' => $model,
					'zone' => $zone,
					'order' => $order
				]);
			}
		}
		return $this->render('order', [
			'model' => $model,
			'zone' => $zone,
			'order' => $order
		]);
	}

	/**
     * Lists all Panel models.
     * @return mixed
     */
    public function actionIndex()
    {
	    $query = Panel::find();
	    $query->joinWith('car');
	    $query->joinWith('driver');
	    $query->all();
	    $dataProvider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => false
	    ]);
		$this->layout = 'main2';
        return $this->render('index', [
//            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionProfile($id)
	{
		if (Yii::$app->request->isAjax)
		{
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$query = (new Query());
//			$sql = $panel->select('*')
//				->leftJoin('car', 'panel.car_id = car.id')
//				->leftJoin('driver', 'panel.driver_id = driver.id')
//				->from('panel')
//				->where('panel.id = '.$id)
//				->createCommand()
//				->sql;
			$panel = $query
				->select('
					panel.id as panel_id,
					panel.car_id as car_id,
					panel.town_center as town_center,
					panel.town as town,
					panel.periferia as periferia,
					panel.km_price as km_price,
					panel.use_single_tariff as use_single_tariff,
					car.*,
					driver.*,
					car_inside.image as car_inside_image,
					car_brand.name as car_brand_name,
					car_class.name as car_class
					')
				->leftJoin('car', 'panel.car_id = car.id')
				->leftJoin('driver', 'panel.driver_id = driver.id')
				->leftJoin('car_inside', 'car.id = car_inside.car_id')
				->leftJoin('car_brand', 'car_brand.id = car.company')
				->leftJoin('car_class', 'car_class.id = car.class')
				->from('panel')
				->where('panel.id = '.$id)
				->one();
			if ($panel)
			{
				return json_encode($panel);
			}
			else
				return json_encode(false);
		}
	}

    /**
     * Displays a single Panel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Panel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Panel();
	    $this->performAjaxValidation($model);
        if ($model->load(Yii::$app->request->post())) {
	        if ($model->validate())
	        {
		        $model->date_create = time();
		        $model->date_update = time();
		        $model->getTrueTariffs();
	        }
	        if ($model->save())
                return $this->redirect('/panel/index');
	        else
		        return $this->render('create', [
			        'model' => $model,
		        ]);
        } else {
	        !$model->station_time ? $model->station_time = 48 : null;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

	public function actionGetCarsTariff()
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if (Yii::$app->request->isAjax)
		{
			$id = $_POST['id'];
			$car = Car::findOne(['id' => $id]);
			if ($car->use_group_tariff)
			{
				$groupTariff = GroupTariff::findOne(['id' => $car->group_tariff_id]);
				$return = json_encode($groupTariff->attributes);
				return $return;
			}
			$model = new \stdClass();
			return json_encode($model);
		}
	}

	public function actionPrint($id, $zone)
	{
		$model = Panel::findOne($id) ? Panel::findOne($id) : Order::findOne($id);
		return $this->renderAjax('print', [
			'model' => $model,
			'zone' => $zone
		]);
	}

    /**
     * Updates an existing Panel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

	    if ($model->load(Yii::$app->request->post())) {
		    $model->date_update = time();
		    if ($model->save())
			    return $this->redirect('/panel/index');
		    else
			    return $this->render('create', [
				    'model' => $model,
			    ]);
	    } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

	public function actionDelete2()
	{
		$model = new PanelDeleteForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate())
		{
			$model = Panel::findOne(['car_id' => $model->car_id]);
			if ($model->delete())
				return $this->redirect('index');
		}
		return $this->render('delete', [
			'model' => $model
		]);
	}
    /**
     * Deletes an existing Panel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionDelete($id)
	{
//		$this->findModel($id)->delete(true);
		$query = Panel::findOne($id);
		if ($query)
			$query->delete(true);
		return $this->redirect('index');
	}

	public function actionAdd()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if (Yii::$app->request->isGet)
		{
			$id = $_GET['id'];
			return Panel::addByCardId($id);
		}
		if (Yii::$app->request->isPost)
		{
			if ($raw_data = $GLOBALS['HTTP_RAW_POST_DATA'])
			{
				$object = json_decode($raw_data);
				$id = $object->id;
				return Panel::addByCardId($id);
			}
			else
			{
				$id = $_POST['id'];
				return Panel::addByCardId($id);
			}
		}
		return json_encode(false);
	}

	public function actionGetInfo()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if (Yii::$app->request->isGet)
		{
			$id = $_GET['id'];
			$return = DriverCar::getFullInfo($id);
			return $return;
		}
		if($raw_data = file_get_contents("php://input"))
		{
			$object = json_decode($raw_data);
			$id = $object->id;
			$return = DriverCar::getFullInfo($id);
		}
		else
			$return = false;
//		$object = json_decode($return);
		return $return;
	}
    /**
     * Finds the Panel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Panel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Panel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function performAjaxValidation($model)
	{
		if (Yii::$app->request->isAjax || Yii::$app->request->isPjax) {
			if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				echo json_encode(ActiveForm::validate($model));
				Yii::$app->end();
			}
		}
	}
}
