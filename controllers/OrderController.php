<?php

namespace app\controllers;

use app\models\PanelSearch;
use Yii;
use app\models\Order;
use app\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
//        ];
//    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch(['user_id' => Yii::$app->user->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionOrder($id)
	{
		$model = $this->findModel($id);
		return $this->render('order', [
			'model' => $model,
//			'zone' => $zone,
//			'order' => $order
		]);
	}

	public function actionProfile($id)
	{
		if (Yii::$app->request->isAjax)
		{
//			Yii::$app->response
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$result = [];
			if ($model = $this->findModel($id))
			{
				$result['html'] = $this->renderPartial('profile', [
					'model' => $model
				]);
				return json_encode($result);
			}
			else
			{
				$result['error'] = 'Профиль не найден на стоянке';
				return json_encode($result);
			}
		}
		else
		{
			$model = $this->findModel($id);
			return $this->renderPartial('profile', [
				'model' => $model
			]);
		}
	}

	protected function findModel($id)
	{
		if (($model = Order::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
