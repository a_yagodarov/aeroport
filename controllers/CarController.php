<?php

namespace app\controllers;

use app\models\CarBlackList;
use app\models\CarInside;
use app\models\CarInsideSearch;
use app\models\DriverCar;
use app\models\DriverCarSearch;
use app\models\DriverSearch;
use Yii;
use app\models\Car;
use app\models\CarSearch;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CarController implements the CRUD actions for Car model.
 */
class CarController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
	        'access' => [
		        'class' => AccessControl::className(),
		        'ruleConfig' => [
			        'class' => AccessRule::className(),
		        ],
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['@'],
				        'matchCallback' => function(){
					        return in_array(Yii::$app->user->identity->role_id, [
						        \app\models\User::ROLE_ADMIN,
//						        \app\models\User::DISPATCHER,
					        ]);
				        }
			        ],
		        ],
	        ]
        ];
    }

	public function actionGetForm()
	{
		$model = new CarBlackList();
		return $this->renderAjax('black_list/form', [
			'model' => $model
		]);
	}

    /**
     * Lists all Car models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Car model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($car_id)
    {
	    $model = $this->findModel($car_id);
		$carInside = (new CarInsideSearch())->search(['car_id' => $model->id]);
        return $this->render('view', [
            'model' => $model,
	        'carInside' => $carInside
        ]);
    }

    /**
     * Creates a new Car model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Car();
		$model->scenario = 'create';
	    $model->loadTariffs();
        if ($model->load(Yii::$app->request->post())) {
	        $image = UploadedFile::getInstance($model, 'image');
	        if ($image)
	        {
		        $model->image = $image;
		        $model->upload();
	        }
	        else
	        {
		        return $this->render('create', [
			        'model' => $model,
		        ]);
	        }
//	        $model->tariffs->load($_POST);
	        if ($model->save())
	        {
//		        $model->tariffs->id = $model->id;
//			    $model->tariffs->save();
		        return $this->redirect('index');
	        }
	        else
		        return $this->render('create', [
			        'model' => $model,
		        ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

	public function actionActivate($car_id)
	{
		$model = $this->findModel($car_id);
		$model->permission_to_drive = 1;
		$model->save(false);
		return $this->redirect('index');
	}

	public function actionDeactivate($car_id)
	{
		$model = $this->findModel($car_id);
		$model->permission_to_drive = 0;
		$model->save(false);
		return $this->redirect('index');
	}

    /**
     * Updates an existing Car model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($car_id)
    {
        $model = $this->findModel($car_id);
	    $model->loadTariffs();
        if ($model->load(Yii::$app->request->post())) {
	        $image = UploadedFile::getInstance($model, 'image');
	        if ($image)
	        {
		        $model->image = $image;
		        $model->upload();
	        }
	        else
	        {
		        $model->image = $model->getOldAttribute('image');
	        }
	        $model->tariffs->load($_POST);
	        if ($model->save())
	        {
		        $model->tariffs->id = $model->id;
		        $model->tariffs->save();
		        return $this->redirect('index');
	        }
	        else return $this->render('update', [
		        'model' => $model,
	        ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

	public function actionDriver($car_id)
	{
		$searchModel = new DriverCarSearch(['car_id' => $car_id]);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('driver/index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
			'car_id' => $car_id
		]);
	}

	public function actionCarInside($car_id)
	{
		$searchModel = new CarInsideSearch(['car_id' => $car_id]);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('car_inside/index',[
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
			'car_id' => $car_id
		]);
	}

    /**
     * Deletes an existing Car model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Car model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Car the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($car_id)
    {
        if (($model = Car::findOne($car_id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
