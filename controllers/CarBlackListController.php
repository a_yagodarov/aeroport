<?php

namespace app\controllers;

use app\models\Car;
use Yii;
use app\models\CarBlackList;
use app\models\CarBlackListSearch;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CarBlackListController implements the CRUD actions for CarBlackList model.
 */
class CarBlackListController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
	        'access' => [
		        'class' => AccessControl::className(),
		        'ruleConfig' => [
			        'class' => AccessRule::className(),
		        ],
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['@'],
				        'matchCallback' => function(){
					        return in_array(Yii::$app->user->identity->role_id, [
						        \app\models\User::ROLE_ADMIN,
						        \app\models\User::DISPATCHER,
					        ]);
				        }
			        ],
		        ],
	        ]
        ];
    }

    /**
     * Lists all CarBlackList models.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $searchModel = new CarBlackListSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }
//
//    /**
//     * Displays a single CarBlackList model.
//     * @param integer $id
//     * @return mixed
//     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new CarBlackList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($car_id)
    {
	    if (!$model = CarBlackList::findOne(['id' => $car_id]))
            $model = new CarBlackList();

        if ($model->load(Yii::$app->request->post())) {
	        $model->id = $car_id;
	        if ($model->save())
	        {
		        $result = Yii::$app->db->createCommand()->update('car', ['is_black_list' => 1], 'id = '.$car_id)->execute();
		        if ($result)
		            return $this->redirect('/car/index');
		        else
			        return $this->render('create', [
				        'model' => $model,
			        ]);
	        }
	        else
		        return $this->render('create', [
			        'model' => $model,
		        ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CarBlackList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing CarBlackList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($car_id)
    {
        $car = Car::findOne(['id' => $car_id]);
		$car->is_black_list = 0;
	    $car->save();
        return $this->redirect(['/car/index']);
    }

    /**
     * Finds the CarBlackList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarBlackList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarBlackList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
