<?php

namespace app\controllers;

use app\models\MessageForm;
use app\models\Order;
use app\models\PanelDeleteForm;
use Yii;
use app\models\Panel;
use app\models\PanelSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ManagerPanelController implements the CRUD actions for Panel model.
 */
class ManagerPanelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
	        'access' => [
		        'class' => AccessControl::className(),
		        'ruleConfig' => [
			        'class' => AccessRule::className(),
		        ],
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['@'],
				        'matchCallback' => function(){
					        return in_array(Yii::$app->user->identity->role_id, [
						        \app\models\User::ROLE_MANAGER,
						        \app\models\User::ROLE_ADMIN
					        ]);
				        }
			        ],
	            ]
	        ]
	        ];
    }

    /**
     * Lists all Panel models.
     * @return mixed
     */
    public function actionIndex()
    {
	    $query = Panel::find();
	    $query->leftJoin(
		    'driver',
		    'panel.driver_id = driver.id'
	    );
	    $query->andFilterWhere([
		    'not in',
		    'driver.rating',
		    0
	    ]);

	    $dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
//        $searchModel = new PanelSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//			$dataProvider->setSort([
//				'attributes' => [
//					'town' => [
//						'asc' => [
//							'cast(town as unsigned)' => SORT_ASC
//						],
//						'desc' => [
//							'cast(town as unsigned)' => SORT_DESC
//	                    ],
//					],
//					'town_center' => [
//						'asc' => [
//							'cast(town_center as unsigned)' => SORT_ASC
//						],
//						'desc' => [
//							'cast(town_center as unsigned)' => SORT_DESC
//						],
//					],
//					'km_price' => [
//						'asc' => [
//							'cast(km_price as unsigned)' => SORT_ASC
//						],
//						'desc' => [
//							'cast(km_price as unsigned)' => SORT_DESC
//						],
//					],
//				]
//			]);
        return $this->renderAjax('index', [
//            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionProfile($id)
	{
		if (Yii::$app->request->isAjax)
		{
//			Yii::$app->response
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$result = [];
			if ($model = $this->findModel($id))
			{
				$result['html'] = $this->renderAjax('profile', [
					'model' => $model
				]);
				return json_encode($result);
			}
			else
			{
				$result['error'] = 'Профиль не найден на стоянке';
				return json_encode($result);
			}
		}
		else
		{
			$model = $this->findModel($id);
			return $this->renderPartial('profile', [
				'model' => $model
			]);
		}
	}

	public function actionPrint($id, $zone)
	{
		$model = $this->findModel($id);
		return $this->renderAjax('print', [
			'model' => $model,
			'zone' => $zone
		]);
	}

	public function actionCreate()
	{
		$model = new Panel();
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post())) {
			if ($model->validate())
			{
				$model->date_create = time();
				$model->date_update = time();
				$model->getTrueTariffs();
			}
			if ($model->save())
				return $this->redirect('index');
			else
				return $this->render('create', [
					'model' => $model,
				]);
		} else {
			!$model->station_time ? $model->station_time = 48 : null;
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionDelete2()
	{
		$model = new PanelDeleteForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate())
		{
			$model = Panel::findOne(['car_id' => $model->car_id]);
			if ($model->delete())
				return $this->redirect('index');
		}
		return $this->render('delete', [
			'model' => $model
		]);
	}

    /**
     * Displays a single Panel model.
     * @param integer $id
     * @return mixed
     */

	/**
	 * Отправляет смс и удаляет из стоянки автомобиль по его id на стоянке и по зоне
	 * @param integer $id
	 * @param integer $zone
	 * @return mixed
	 */
	public function actionOrder($id, $zone)
	{
		$model = $this->findModel($id);
		$order = new MessageForm([
			'zone' => $zone,
		]);
		if (Yii::$app->request->isAjax)
		{
			$order->zone = $zone;
			$order->panel_id = $id;
			$order->place = Yii::$app->user->identity->terminal_id;
			if ($order->validate() && $order->sendMessage())
			{
				return $this->redirect([
					'delete',
					'id' => $id,
				]);
			}
			else
			{
				return $this->render('order', [
					'model' => $model,
					'zone' => $zone,
					'order' => $order
				]);
			}
		}
		return $this->render('order', [
			'model' => $model,
			'zone' => $zone,
			'order' => $order
		]);
	}

	public function actionSendSms($id, $zone)
	{
		$MessageForm = new MessageForm([
			'zone' => $zone,
			'place' => Yii::$app->user->identity->terminal_id,
			'panel_id' => $id
		]);

		$return = $MessageForm->sendMessage();

		return $return;
	}

    /**
     * Deletes an existing Panel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    $this->findModel($id)->delete(true);

	    return $this->redirect(['index']);
    }

    /**
     * Finds the Panel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Panel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Panel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	protected function performAjaxValidation($model)
	{
		if (Yii::$app->request->isAjax || Yii::$app->request->isPjax) {
			if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				echo json_encode(ActiveForm::validate($model));
				Yii::$app->end();
			}
		}
	}
}
